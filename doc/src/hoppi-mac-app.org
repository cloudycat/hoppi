# -*- org -*-

#+TITLE: Hoppi Mac Application
#+AUTHOR: Paul Onions
#+EMAIL: paul.onions@acm.org

* Introduction
** Features
Hoppi is an application for Apple macOS to allow communication with
HP48/49/50-series calculators using the Kermit file transfer protocol.
It supports both USB and RS232 calculator ports.

The application implements a Kermit server and presents a simple
interface to the user allowing the state of the connection to be
controlled and monitored.  A key focus of the app is to allow the
entire file transfer process to be controlled by the calculator, hence
features are provided to allow the calculator to perform folder and
file selection independently of the computer.

In addition, to support the character set used by these calculators
(which includes mathematical symbols as well as the usual alphanumeric
characters), calculator character codes are mapped automatically to
Unicode UTF8 codes on the computer (both file names and file content).
The particular mapping used is compatible with that described at
[[http://www.hpmuseum.org/cgi-sys/cgiwrap/hpmuseum/articles.cgi?read=1218][Character Map]].


* Using Hoppi
** The Main Window
The Hoppi application main window is shown in the following figure,
displaying its state after transferring a file called ``→dB'' from the
calculator to the computer.

#+CAPTION: Hoppi main window
[[file:hoppi-window-screenshot.png]]

The main window is composed of three main areas:-
1. across the top of the window is the current workspace folder path,
2. on the left is the calculator status panel, and
3. on the right is the Kermit protocol status panel.

The current workspace folder is the folder on the computer from/to
which all file transfers will be made.  In this case it shows the
``Documents/RPL/'' folder of my home folder.  Click on any path
component to open a dialog allowing you to choose a new folder.  To
allow files in this folder to be overwritten by transfers from the
calculator tick the ``Allow Overwrite'' checkbox.

The calculator status panel, on the left-hand side of the window,
shows the current state of the connection to the calculator.  There
are two modes of operation: USB or Serial.  If USB is selected then
Hoppi will attempt to communicate with the calculator via the
calculator's USB port, and will show ``calculator detected'' when the
calculator is attached and switched on.  In this mode it will ignore
the serial device choose-box.  Alternatively, if Serial is selected,
then Hoppi will try to communicate with the calculator's serial port,
using the serial device selected in the drop-down choose-box.

*Note:* serial adaptor cables often connect to the computer via a USB
port.  However, the combination of cable and driver will appear as a
serial device to any applications running on macOS, so to use one of
these cables the Serial option should be selected.  This applies, in
particular, to those special all-in-one cables that connect the
computer's USB port to an HP48 serial port.  In short, select the
option that corresponds to the /calculator/ port that is being used.

The Kermit protocol status panel, on the right-hand side of the
window, shows what is happening in the current transfer.  In case of
problems you can reset the state of the Kermit server by pressing the
Reset button.

** The Packet Logger Window
The packet logger window, accessible from the View menu, displays the
contents of packets as they are sent and received.

#+CAPTION: Packet logger window
[[file:packet-logger-screenshot.png]]

The contents of each packet are displayed on a single line in five fields.
1. a right-pointing arrow for a packet received from the calculator, a
   left-pointing arrow for a packet sent to the calculator,
2. the sequence number of the packet,
3. the Kermit packet type (denoted by a single letter),
4. the data contents of the packet (enclosed in hollow brackets), and
5. the packet checksum (enclosed in hollow brackets).

In addition, ticking the ``Show Hex'' checkbox shows the data and
checksum contents of each packet as sequences of hexadecimal byte
values on a second line.

** The Message Logger Window
As well as transferring files, the Kermit protocol supports the
sending and receiving of arbitrary text messages.  These are initiated
by the sending of special ``X'' packets, as opposed to file transfers
which are initiated by ``F'' packets.  The message logger window can
be used to examine all such Kermit messages, both inbound and
outbound.

#+CAPTION: Message logger window
[[file:message-logger-screenshot.png]]

In addition to these X-packet messages, Hoppi also treats unnamed file
transfers as message transfers.  That is, file transfers initiated by
an F-packet that contains a zero-length filename.  In this way it is
easy to generate such messages on the calculator using the HSDNO and
HSNDS Hoppi RPL Library commands -- simply specify an empty string as
filename (see the [[file:hoppi-rpl-lib.org][Hoppi RPL Library]]
documentation for details of these commands).

The reason for wanting to be able to send messages from the calculator
to the computer is that this is the mechanism by which GROB files are
transferred to the GROB viewer window, as described in the next section.

** The GROB Viewer Window
The GROB viewer window, accessible from the View menu, allows GROB
object messages to be displayed.

#+CAPTION: GROB viewer window
[[file:grob-viewer-screenshot.png]]

To send a GROB object from the calculator to the computer, it should
be recalled to the stack then sent to the computer with the following
command sequence (see [[file:hoppi-rpl-lib.org][Hoppi RPL Library]])
#+BEGIN_EXAMPLE
  "" HSNDO
#+END_EXAMPLE
this sends the GROB as an unnamed file, which will be intercepted by
Hoppi and displayed in the viewer window.  See the previous section
for details of message transfers in Hoppi.

The image seen in the viewer window can be pasted into other macOS
applications by selecting Copy from the Edit menu then pasting into
the other application as normal.  In particular, the Preview
application features a ``New from Clipboard'' feature in the File menu
which can be used to export the image to the file format of your choice.

** Transferring Files
The calculator SEND, RECV and KGET commands can be used to transfer
calculator objects to and from the computer, as they can with any
Kermit server.

For example, to send a calculator object stored in global variable
PGM1 to the computer, issue the command
#+BEGIN_EXAMPLE
  'PGM1' SEND
#+END_EXAMPLE
on the calculator.  And to transfer it back again, storing it in the
current calculator directory, do
#+BEGIN_EXAMPLE
  'PGM1' KGET
#+END_EXAMPLE
or possibly instead
#+BEGIN_EXAMPLE
  RECV
#+END_EXAMPLE
followed by invoking ``Send File...'' on the computer (from the File
menu) and choosing the ``PGM1'' file in the current workspace folder.

Alternatively, these calculator commands can be invoked in the FILER
on an HP49G+/HP50G, or in the ``Transfer'' program on an HP48
calculator (accessible via right-shift I/O on the number 1 key).  The
latter can also be used to manage the computer's current workspace
folder completely from the calculator.  Note however that its
equivalent on the HP49G+/HP50G has problems with these extended
features, and instead the [[file:hoppi-rpl-lib.org][Hoppi RPL Library]] can be used.

In addition to transferring calculator objects Hoppi can be used to
transfer the contents of arbitrary text files to and from the
calculator, where they will appear as string objects.  To send the
contents of a text file to the calculator select the "Send File as
String..." function from the File menu, choose the appropriate file,
then issue the RECV command on the calculator.  This will create a
variable in the current calculator directory (named according to the
file name) holding a string object corresponding to the contents of
the text file.

To send the contents of a string object to the computer as a text
file, select the "Receive File as String" function from the File menu
then issue the SEND command on the calculator.  This will write the
contents of the string to a file on the computer, the file being named
according to the name of the sent variable on the calculator.

Note that only the very next transfer will be treated as a "text file
transfer".  Subsequent transfers after this will revert to standard
calculator object transfers.  This can be seen by noting the the
"Receive File as String" menu item will become ticked after first
being selected, then become unticked when the transfer has taken
place.  To cancel a pending text file transfer simply select the
"Receive File as String" menu item again and it will become unticked.

This text file transfer can also be achieved, under control of the
calculator, using the HGETS command from the
[[file:hoppi-rpl-lib.org][Hoppi RPL Library]].  The reverse process,
sending the contents of a string object to a text file on the
computer, can be accomplished using the complementary HSNDS command
from the library.


* Obtaining Hoppi
** Downloading a Release
Hoppi is freely available from
[[http://bitbucket.org/cloudycat/hoppi/downloads]] where you should find
versions for PowerPC and Intel Macs.  The PowerPC version requires
macOS 10.5 (Leopard), the Intel version requires 10.7 (Lion) or better.

** Building from Source
Hoppi is an open source program with an MIT-style licence (see the
LICENCE file for further details).  The easiest way to obtain the
source code is to clone the Hoppi version control repository.  This is
a Mercurial repository hosted at [[http://bitbucket.org/cloudycat]].
To clone it to your computer issue the command
#+BEGIN_EXAMPLE
hg clone http://bitbucket.org/cloudycat/hoppi
#+END_EXAMPLE
in a Terminal session.

Hoppi is written in Clozure Common Lisp (CCL), an implementation of
the Common Lisp language that supports direct interaction with the
Objective-C runtime on macOS (see http://ccl.clozure.com for more
information).  In addition a small C library, libhoppi-io, is used for
interfacing with the Mac's IOKit framework.  It is called from, and
calls back into, the Lisp environment.

To build Hoppi, build the C library first using Xcode and copy the
generated libhoppi-io.dylib file to the src/lib/hoppi-io/
directory. Then generate NIB files from the included XIB files (using
Interface Builder on older macOS or Xcode on newer).  Finally build the
application itself from the CCL IDE (see the file app-build.lisp) and
install it, like any other app, by copying it to your Applications
folder.


* Connectivity
For reference, for communication with the HP48 I have used the USB
cable sold by eTek-Plus on eBay (http://stores.ebay.com/eTek-Plus),
also available from Samson Cables (http://www.samsoncables.com), and I
have also used an FTDI US232R-10 USB-serial convertor cable (along
with a standard HP48 serial cable).


* Known Issues
** HP49G+/HP50G Transfer program
Hoppi has been tested with an HP48GX and an HP49G+ calculator where
file transfer seems reliable on both.  Furthermore, the HP48GX
"Transfer" program (accessible via Left-shift I/O) works to its full
extent, allowing retrieval of directory listings and remote control of
Hoppi's current workspace folder.  However, the HP49G+ "Transfer"
program (accessible via APPS->I/O functions->Transfer) seems to have
some problems using these extended features, sometimes generating
nonsensical Kermit packets.  It is for this reason that I have
developed a simple RPL library/application for file transfer on the
HP49G+/HP50G calculators, known also as Hoppi.  It is included with
the main Hoppi application download and has its own
[[file:hoppi-rpl-lib.org][documentation]].  It is optional, you don't need
it to perform basic file transfer, but I find it useful for managing
transfers, and it is also HP48G compatible.

Alternatively, you can use the standard built-in SEND, RECV and KGET
commands directly (or within the "File Manager" program) on the
calculator, possibly also with my HRCD and HRLD RPL commands (see the
[[file:hoppi-rpl-lib.org][Hoppi RPL library documentation]] for details).

** macOS 10.5 PowerPC
The Copy menu item for the GROB viewer has been disabled when built
with Clozure CL 1.6 on PowerPC, due to a problem with the application
crashing.  I don't understand why this happens, but hopefully it will
not inconvenience too many people.


* Version History
** Hoppi Mac App Version 1.4
+  Now builds on CCL 1.6 again, so support for PowerPC Macs is back.
+  Hoppi-side transfer timeout is extended to 20 seconds to ensure
   very large objects can be transferred to the calculator (the kermit
   implementation on HP48 calculators slows down as the transfer
   progresses).
+  Updating in the GUI of the number of bytes sent is now performed on
   a per-packet basis, instead of in 1024 byte bursts.

** Hoppi Mac App Version 1.3.4
+  Now builds with Clozure CL 1.11 and so works on macOS Sierra (10.12).
   Will not build with versions of CCL below 1.11, so will no longer
   build on PowerPC Macs.

** Hoppi Mac App Version 1.3.3
+  Fixed handling of Kermit "F" generic command (G packet, F command).

** Hoppi Mac App Version 1.3.2
+  Fixed handling of file and folder names containing a semicolon.

** Hoppi Mac App Version 1.3.1
+  Corrected location of libhoppi-io.dylib in the application bundle.
   Now works on OSX 10.10.3.

** Hoppi Mac App Version 1.3
+  Added the message logger window.
+  Added the GROB viewer window.

** Hoppi Mac App Version 1.2
+  Added the ability to send and receive text files as calculator strings.
+  Included the Hoppi RPL Library version 0.4.
+  Improved documentation.

** Hoppi Mac App Version 1.1
+  Now automatically re-opens last successfully opened serial device.
+  Added support for Kermit's generic C and D commands.
+  Added HRCD and HRLD UserRPL utility commands.
+  Corrected handling of single-character directory names.

** Hoppi Mac App Version 1.0
+  Initial stable version.


* Coda
Finally, why is it called Hoppi?  Take the capital letters from "HP
Packet I/O" and rearrange them.  Plus it sounds frog-like (for
Kermit).  Well, I didn't say it was profound...


-----------------------------------------------------------------------
