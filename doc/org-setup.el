;;;
;;; Copyright 2015 Paul Onions
;;; All rights reserved.
;;;
;;; Org-mode publishing setup for the Hoppi documentation.
;;;
;;; For org-mode version 8.  Load this file into Emacs-Lisp
;;; (or `eval-buffer' it) then do `org-publish'.
;;;
(require 'ox-publish)

(defvar hoppi-doc-dir (file-name-directory (or load-file-name
                                               (buffer-file-name (current-buffer)))))

(add-to-list 'org-publish-project-alist
             (list "hoppi-help-org"
		   :base-directory (concat hoppi-doc-dir "src/")
		   :base-extension "org"
		   :publishing-directory (concat hoppi-doc-dir "../src/gui/hoppi-help/")
		   :recursive t
		   :publishing-function 'org-html-publish-to-html
		   :auto-preamble t
		   :auto-postamble t
		   :auto-sitemap t
		   :export-author-info "Paul Onions"
		   :table-of-contents nil
		   :section-numbers nil))

(add-to-list 'org-publish-project-alist
             (list "hoppi-help-static"
		   :base-directory (concat hoppi-doc-dir "src/")
		   :base-extension "html\\|css\\|js\\|png\\|jpg\\|gif\\|pdf\\|svg"
		   :publishing-directory (concat hoppi-doc-dir "../src/gui/hoppi-help/")
		   :recursive t
		   :publishing-function 'org-publish-attachment))

(add-to-list 'org-publish-project-alist
             '("hoppi-help" :components ("hoppi-help-org" "hoppi-help-static")))
