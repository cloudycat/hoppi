//
// Copyright 2012 - 2013 Paul Onions
//
// See LICENCE file for copying terms.
//
// Low-level USB I/O routines for the Hoppi project
//

//
// Includes
//
#include <mach/mach.h>

#include <CoreFoundation/CoreFoundation.h>

#include <IOKit/IOKitLib.h>
#include <IOKit/IOCFPlugIn.h>
#include <IOKit/usb/IOUSBLib.h>

#include "hoppi-io.h"

// USB constants
#define HP49GP_USB_VENDOR_ID  0x3F0
#define HP49GP_USB_PRODUCT_ID 0x121

//
// Global variables
//
IONotificationPortRef usb_notification_port;

io_iterator_t usb_arrivals_iterator;
io_iterator_t usb_departures_iterator;

void (*usb_post_configure_lisp_callback)(void) = NULL;
void (*usb_post_unconfigure_lisp_callback)(void) = NULL;

IOUSBDeviceInterface **usb_di = NULL;
IOUSBInterfaceInterface **usb_ii = NULL;

UInt8 incoming_usb_endpoint_num = 0;
UInt8 outgoing_usb_endpoint_num = 0;

//
// unconfigure_usb_device
//
void unconfigure_usb_device() {
  if (usb_ii) {
    (*usb_ii)->USBInterfaceClose(usb_ii);
    (*usb_ii)->Release(usb_ii);
    usb_ii = NULL;
  }
  
  if (usb_di) {
    (*usb_di)->USBDeviceClose(usb_di);
    (*usb_di)->Release(usb_di);
    usb_di = NULL;
  }
}

//
// configure_usb_device
//
int configure_usb_device(io_service_t obj) {
  kern_return_t kr;
  IOCFPlugInInterface **pii = NULL;
  IOUSBDeviceInterface **temp_di = NULL;
  HRESULT result;
  SInt32 score;
  UInt16 vendor, product;
  UInt8 num_configs;
  IOUSBConfigurationDescriptorPtr config_descriptor;
  IOUSBFindInterfaceRequest request;
  io_iterator_t iterator;
  io_service_t iobj;
  UInt8 interface_num_endpoints;
  UInt8  pipe_1_direction, pipe_1_number, pipe_1_transfer_type, pipe_1_interval;
  UInt16 pipe_1_max_packet_size;
  UInt8  pipe_2_direction, pipe_2_number, pipe_2_transfer_type, pipe_2_interval;
  UInt16 pipe_2_max_packet_size;
  bool found_wanted_interface = FALSE;
  CFRunLoopSourceRef run_loop_source;

  // Create intermediate plugin
  kr = IOCreatePlugInInterfaceForService(obj, kIOUSBDeviceUserClientTypeID,
                                         kIOCFPlugInInterfaceID, &pii, &score);
  IOObjectRelease(obj);

  if ((kr != kIOReturnSuccess) || !pii) {
    return CANNOT_CREATE_PLUGIN_INTERFACE;
  }

  // Create the device interface
  result = (*pii)->QueryInterface(pii, CFUUIDGetUUIDBytes(kIOUSBDeviceInterfaceID), (LPVOID *)&temp_di);
  (*pii)->Release(pii);

  if (result || !temp_di) {
    return CANNOT_CREATE_DEVICE_INTERFACE;
  }

  // Verify vendor and product IDs
  kr = (*temp_di)->GetDeviceVendor(temp_di, &vendor);
  if ((kr != kIOReturnSuccess) || (vendor != HP49GP_USB_VENDOR_ID)) {
    (*temp_di)->Release(temp_di);
    return WRONG_VENDOR_ID;
  }

  kr = (*temp_di)->GetDeviceProduct(temp_di, &product);
  if ((kr != kIOReturnSuccess) || (product != HP49GP_USB_PRODUCT_ID)) {
    (*temp_di)->Release(temp_di);
    return WRONG_PRODUCT_ID;
  }

  // Commit to the new device, unconfigure the existing one first
  unconfigure_usb_device();
  usb_di = temp_di;
  usb_ii = NULL;

  // Open device
  kr = (*usb_di)->USBDeviceOpen(usb_di);
  if (kr != kIOReturnSuccess) {
    (*usb_di)->Release(usb_di);
    usb_di = NULL;
    usb_ii = NULL;
    return CANNOT_OPEN_DEVICE;
  }

  // Get number of configurations
  kr = (*usb_di)->GetNumberOfConfigurations(usb_di, &num_configs);
  if (kr != kIOReturnSuccess) {
    (*usb_di)->USBDeviceClose(usb_di);
    (*usb_di)->Release(usb_di);
    usb_di = NULL;
    usb_ii = NULL;
    return CANNOT_GET_NUMBER_OF_CONFIGURATIONS;
  }

  // Check there is at least one configuration
  if (num_configs == 0) {
    (*usb_di)->USBDeviceClose(usb_di);
    (*usb_di)->Release(usb_di);
    usb_di = NULL;
    usb_ii = NULL;
    return NO_CONFIGURATIONS_AVAILABLE;
  }

  // Get configuration 0 descriptor
  kr = (*usb_di)->GetConfigurationDescriptorPtr(usb_di, 0, &config_descriptor);
  if (kr != kIOReturnSuccess) {
    (*usb_di)->USBDeviceClose(usb_di);
    (*usb_di)->Release(usb_di);
    usb_di = NULL;
    usb_ii = NULL;
    return CANNOT_GET_CONFIGURATION_0_DESCRIPTOR;
  }

  // Set configuration to configuration 0
  kr = (*usb_di)->SetConfiguration(usb_di, config_descriptor->bConfigurationValue);
  if (kr != kIOReturnSuccess) {
    (*usb_di)->USBDeviceClose(usb_di);
    (*usb_di)->Release(usb_di);
    usb_di = NULL;
    usb_ii = NULL;
    return CANNOT_SET_CONFIGURATION;
  }

  // Find interface
  request.bInterfaceClass    = kIOUSBFindInterfaceDontCare;
  request.bInterfaceSubClass = kIOUSBFindInterfaceDontCare;
  request.bInterfaceProtocol = kIOUSBFindInterfaceDontCare;
  request.bAlternateSetting  = kIOUSBFindInterfaceDontCare;

  kr = (*usb_di)->CreateInterfaceIterator(usb_di, &request, &iterator);
  if (kr != kIOReturnSuccess) {
    (*usb_di)->USBDeviceClose(usb_di);
    (*usb_di)->Release(usb_di);
    usb_di = NULL;
    usb_ii = NULL;
    return CANNOT_CREATE_INTERFACE_ITERATOR;
  }

  // Iterate over available interfaces
  while ((iobj = IOIteratorNext(iterator))) {
    // Create the plugin
    kr = IOCreatePlugInInterfaceForService(iobj, kIOUSBInterfaceUserClientTypeID,
                                           kIOCFPlugInInterfaceID, &pii, &score);
    IOObjectRelease(iobj);

    if ((kr != kIOReturnSuccess) || !pii) {
      continue;
    }

    if (!found_wanted_interface) {
      // Create the interface-interface
      result = (*pii)->QueryInterface(pii, CFUUIDGetUUIDBytes(kIOUSBInterfaceInterfaceID), (LPVOID *) &usb_ii);
      (*pii)->Release(pii);

      if (result || !usb_ii) {
        usb_ii = NULL;
        continue;
      }

      found_wanted_interface = TRUE;

      // Open the interface
      kr = (*usb_ii)->USBInterfaceOpen(usb_ii);
      if (kr != kIOReturnSuccess) {
        (*usb_ii)->Release(usb_ii);
        usb_ii = NULL;
        (*usb_di)->USBDeviceClose(usb_di);
        (*usb_di)->Release(usb_di);
        usb_di = NULL;
        return CANNOT_OPEN_INTERFACE;
      }

      // Get the number of endpoints of this interface
      kr = (*usb_ii)->GetNumEndpoints(usb_ii, &interface_num_endpoints);
      if (kr != kIOReturnSuccess) {
        (*usb_ii)->USBInterfaceClose(usb_ii);
        (*usb_ii)->Release(usb_ii);
        usb_ii = NULL;
        (*usb_di)->USBDeviceClose(usb_di);
        (*usb_di)->Release(usb_di);
        usb_di = NULL;
        return CANNOT_GET_NUMBER_OF_ENDPOINTS;
      }

      if (interface_num_endpoints != 2) {
        (*usb_ii)->USBInterfaceClose(usb_ii);
        (*usb_ii)->Release(usb_ii);
        usb_ii = NULL;
        (*usb_di)->USBDeviceClose(usb_di);
        (*usb_di)->Release(usb_di);
        usb_di = NULL;
        return WRONG_NUMBER_OF_ENDPOINTS;
      }

      // Add async port
      kr = (*usb_ii)->CreateInterfaceAsyncEventSource(usb_ii, &run_loop_source);
      if (kr != kIOReturnSuccess) {
        (*usb_ii)->USBInterfaceClose(usb_ii);
        (*usb_ii)->Release(usb_ii);
        usb_ii = NULL;
        (*usb_di)->USBDeviceClose(usb_di);
        (*usb_di)->Release(usb_di);
        usb_di = NULL;
        return CANNOT_CREATE_ASYNC_EVENT_SOURCE;
      }
      CFRunLoopAddSource(CFRunLoopGetCurrent(), run_loop_source, kCFRunLoopDefaultMode);
    
      // Examine endpoint 1
      kr = (*usb_ii)->GetPipeProperties(usb_ii, 1, &pipe_1_direction, &pipe_1_number,
                                        &pipe_1_transfer_type, &pipe_1_max_packet_size, &pipe_1_interval);
      if (kr != kIOReturnSuccess) {
        (*usb_ii)->USBInterfaceClose(usb_ii);
        (*usb_ii)->Release(usb_ii);
        usb_ii = NULL;
        (*usb_di)->USBDeviceClose(usb_di);
        (*usb_di)->Release(usb_di);
        usb_di = NULL;
        return CANNOT_EXAMINE_ENDPOINT_1;
      }

      // Examine endpoint 2
      kr = (*usb_ii)->GetPipeProperties(usb_ii, 2, &pipe_2_direction, &pipe_2_number,
                                        &pipe_2_transfer_type, &pipe_2_max_packet_size, &pipe_2_interval);
      if (kr != kIOReturnSuccess) {
        (*usb_ii)->USBInterfaceClose(usb_ii);
        (*usb_ii)->Release(usb_ii);
        usb_ii = NULL;
        (*usb_di)->USBDeviceClose(usb_di);
        (*usb_di)->Release(usb_di);
        usb_di = NULL;
        return CANNOT_EXAMINE_ENDPOINT_2;
      }

      // Check for an IN endpoint
      if ((pipe_1_direction != kUSBIn) && (pipe_2_direction != kUSBIn)) {
        (*usb_ii)->USBInterfaceClose(usb_ii);
        (*usb_ii)->Release(usb_ii);
        usb_ii = NULL;
        (*usb_di)->USBDeviceClose(usb_di);
        (*usb_di)->Release(usb_di);
        usb_di = NULL;
        return NO_IN_ENDPOINT;
      }

      // Check for an OUT endpoint
      if ((pipe_1_direction != kUSBOut) && (pipe_2_direction != kUSBOut)) {
        (*usb_ii)->USBInterfaceClose(usb_ii);
        (*usb_ii)->Release(usb_ii);
        usb_ii = NULL;
        (*usb_di)->USBDeviceClose(usb_di);
        (*usb_di)->Release(usb_di);
        usb_di = NULL;
        return NO_OUT_ENDPOINT;
      }

      // Assign IN and OUT endpoints
      if (pipe_1_direction == kUSBIn) {
        incoming_usb_endpoint_num = 1;
        outgoing_usb_endpoint_num = 2;
      } else {
        incoming_usb_endpoint_num = 2;
        outgoing_usb_endpoint_num = 1;
      }
    }
  }

  // If we're here then we've opened the device but not necessarily found
  // the interface we wanted
  if (!found_wanted_interface) {
    usb_ii = NULL;
    (*usb_di)->USBDeviceClose(usb_di);
    (*usb_di)->Release(usb_di);
    usb_di = NULL;
    return NO_WANTED_INTERFACE_FOUND;
  }

  return SUCCESS;
}

//
// handle_usb_arrivals
//
void handle_usb_arrivals(void *user_data, io_iterator_t iterator) {
  io_service_t obj;
  int rslt;

  while ((obj = IOIteratorNext(iterator))) {
    rslt = configure_usb_device(obj);
    if (rslt == SUCCESS) {
      usb_post_configure_lisp_callback();
    }
  }
}

//
// handle_usb_departures
//
void handle_usb_departures(void *user_data, io_iterator_t iterator) {
  io_service_t obj;

  while ((obj = IOIteratorNext(iterator))) {
    unconfigure_usb_device();
    IOObjectRelease(obj);
    usb_post_unconfigure_lisp_callback();
  }
}

//
// post_usb_async_read
//
int post_usb_async_read(char *buffer, int buffer_len, void *callback_fn, void *callback_data) {
  if (usb_ii) {
    return (int)((*usb_ii)->ReadPipeAsync(usb_ii, incoming_usb_endpoint_num, (void *)buffer, (UInt32)buffer_len, (IOAsyncCallback1)callback_fn, callback_data));
  } else {
    return -1;
  }
}

//
// post_usb_async_write
//
int post_usb_async_write(char *buffer, int buffer_len, void *callback_fn, void *callback_data) {
  if (usb_ii) {
    return (int)((*usb_ii)->WritePipeAsync(usb_ii, outgoing_usb_endpoint_num, (void *)buffer, (UInt32)buffer_len, (IOAsyncCallback1)callback_fn, callback_data));
  } else {
    return -1;
  }
}

//
// setup_usb_io
//
int setup_usb_io (void (*post_configure_callback_fn)(void), void (*post_unconfigure_callback_fn)(void)) {
  mach_port_t master_port;
  CFMutableDictionaryRef matching_dict;
  CFRunLoopSourceRef run_loop_source;
  kern_return_t kr;
  SInt32 vendor_id  = HP49GP_USB_VENDOR_ID;
  SInt32 product_id = HP49GP_USB_PRODUCT_ID;

  // Initialise arrivals and departures lisp callback functions
  usb_post_configure_lisp_callback = post_configure_callback_fn;
  usb_post_unconfigure_lisp_callback = post_unconfigure_callback_fn;
  
  // Create master port for communication with the I/O Kit
  kr = IOMasterPort(MACH_PORT_NULL, &master_port);
  if ((kr != kIOReturnSuccess) || !master_port) {
    return CANNOT_CREATE_IOKIT_MASTER_PORT;
  }

  // Setup matching dictionary for the USB device
  matching_dict = IOServiceMatching(kIOUSBDeviceClassName);
  if (!matching_dict) {
    mach_port_deallocate(mach_task_self(), master_port);
    return CANNOT_CREATE_MATCHING_DICTIONARY;
  }

  CFDictionarySetValue(matching_dict, CFSTR(kUSBVendorName),
                       CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &vendor_id));

  CFDictionarySetValue(matching_dict, CFSTR(kUSBProductName),
                       CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &product_id));

  // Setup a notification port
  usb_notification_port = IONotificationPortCreate(master_port);
  run_loop_source = IONotificationPortGetRunLoopSource(usb_notification_port);
  CFRunLoopAddSource(CFRunLoopGetCurrent(), run_loop_source, kCFRunLoopDefaultMode);

  // Setup a notification for when matching devices arrive
  matching_dict = (CFMutableDictionaryRef)CFRetain(matching_dict);  // next call consumes a reference
  kr = IOServiceAddMatchingNotification(usb_notification_port, kIOFirstMatchNotification, matching_dict,
                                        handle_usb_arrivals, NULL, &usb_arrivals_iterator);
  if (kr != kIOReturnSuccess) {
    CFRelease(matching_dict);
    mach_port_deallocate(mach_task_self(), master_port);
    return CANNOT_ADD_ARRIVAL_NOTIFICATION;
  }
    
  handle_usb_arrivals(NULL, usb_arrivals_iterator);  // arm notification

  // Setup a notification for when matching devices depart
  matching_dict = (CFMutableDictionaryRef)CFRetain(matching_dict);  // next call consumes a reference
  kr = IOServiceAddMatchingNotification(usb_notification_port, kIOTerminatedNotification, matching_dict,
                                        handle_usb_departures, NULL, &usb_departures_iterator);

  if (kr != kIOReturnSuccess) {
    CFRelease(matching_dict);
    mach_port_deallocate(mach_task_self(), master_port);
    return CANNOT_ADD_DEPARTURE_NOTIFICATION;
  }

  handle_usb_departures(NULL, usb_departures_iterator);  // arm notification

  // Finished with the matching dictionary
  CFRelease(matching_dict);

  // Finished with the master port
  mach_port_deallocate(mach_task_self(), master_port);

  return SUCCESS;
}


//
// teardown_usb_io
//
void teardown_usb_io () {
  unconfigure_usb_device();

  IOObjectRelease(usb_arrivals_iterator);
  IOObjectRelease(usb_departures_iterator);

  IONotificationPortDestroy(usb_notification_port);
}
