//
// Copyright 2012 - 2013 Paul Onions
//
// See LICENCE file for copying terms.
//
// Low-level serial I/O routines for the Hoppi project
//

//
// Includes
//
#include <mach/mach.h>

#include <CoreFoundation/CoreFoundation.h>

#include <IOKit/IOKitLib.h>
#include <IOKit/IOCFPlugIn.h>
#include <IOKit/serial/IOSerialKeys.h>
#include <IOKit/IOBSD.h>

#include "hoppi-io.h"

//
// Global variables
//
IONotificationPortRef serial_notification_port;

io_iterator_t serial_arrivals_iterator;
io_iterator_t serial_departures_iterator;

void (*serial_arrivals_lisp_callback)(CFTypeRef bsd_path) = NULL;
void (*serial_departures_lisp_callback)(CFTypeRef bsd_path) = NULL;

//
// handle_serial_arrivals
//
void handle_serial_arrivals(void *user_data, io_iterator_t iterator) {
  io_service_t obj;
  CFTypeRef bsd_path;

  while ((obj = IOIteratorNext(iterator))) {
    bsd_path = IORegistryEntryCreateCFProperty(obj, CFSTR(kIOCalloutDeviceKey),
                                               kCFAllocatorDefault, 0);
    if (bsd_path) {
      serial_arrivals_lisp_callback(bsd_path);
      CFRelease(bsd_path);
    }

    IOObjectRelease(obj);
  }
}

//
// handle_serial_departures
//
void handle_serial_departures(void *user_data, io_iterator_t iterator) {
  io_service_t obj;
  CFTypeRef bsd_path;

  while ((obj = IOIteratorNext(iterator))) {
    bsd_path = IORegistryEntryCreateCFProperty(obj, CFSTR(kIOCalloutDeviceKey),
                                               kCFAllocatorDefault, 0);
    if (bsd_path) {
      serial_departures_lisp_callback(bsd_path);
      CFRelease(bsd_path);
    }

    IOObjectRelease(obj);
  }
}

//
// setup_serial_io
//
int setup_serial_io (void (*arrivals_callback_fn)(CFTypeRef bsd_path), void (*departures_callback_fn)(CFTypeRef bsd_path)) {
  mach_port_t master_port;
  CFMutableDictionaryRef matching_dict;
  CFRunLoopSourceRef run_loop_source;
  kern_return_t kr;

  // Initialise arrivals and departures lisp callback functions
  serial_arrivals_lisp_callback = arrivals_callback_fn;
  serial_departures_lisp_callback = departures_callback_fn;
  
  // Create master port for communication with the I/O Kit
  kr = IOMasterPort(MACH_PORT_NULL, &master_port);
  if ((kr != kIOReturnSuccess) || !master_port) {
    return CANNOT_CREATE_IOKIT_MASTER_PORT;
  }

  // Setup matching dictionary for the serial device
  matching_dict = IOServiceMatching(kIOSerialBSDServiceValue);
  if (!matching_dict) {
    mach_port_deallocate(mach_task_self(), master_port);
    return CANNOT_CREATE_MATCHING_DICTIONARY;
  }

  CFDictionarySetValue(matching_dict, CFSTR(kIOSerialBSDTypeKey), CFSTR(kIOSerialBSDRS232Type));

  // Setup a notification port
  serial_notification_port = IONotificationPortCreate(master_port);
  run_loop_source = IONotificationPortGetRunLoopSource(serial_notification_port);
  CFRunLoopAddSource(CFRunLoopGetCurrent(), run_loop_source, kCFRunLoopDefaultMode);

  // Setup a notification for when matching devices arrive
  matching_dict = (CFMutableDictionaryRef)CFRetain(matching_dict);  // next call consumes a reference
  kr = IOServiceAddMatchingNotification(serial_notification_port, kIOFirstMatchNotification, matching_dict,
                                        handle_serial_arrivals, NULL, &serial_arrivals_iterator);
  if (kr != kIOReturnSuccess) {
    CFRelease(matching_dict);
    mach_port_deallocate(mach_task_self(), master_port);
    return CANNOT_ADD_ARRIVAL_NOTIFICATION;
  }
    
  handle_serial_arrivals(NULL, serial_arrivals_iterator);  // arm notification

  // Setup a notification for when matching devices depart
  matching_dict = (CFMutableDictionaryRef)CFRetain(matching_dict);  // next call consumes a reference
  kr = IOServiceAddMatchingNotification(serial_notification_port, kIOTerminatedNotification, matching_dict,
                                        handle_serial_departures, NULL, &serial_departures_iterator);

  if (kr != kIOReturnSuccess) {
    CFRelease(matching_dict);
    mach_port_deallocate(mach_task_self(), master_port);
    return CANNOT_ADD_DEPARTURE_NOTIFICATION;
  }

  handle_serial_departures(NULL, serial_departures_iterator);  // arm notification

  // Finished with the matching dictionary
  CFRelease(matching_dict);

  // Finished with the master port
  mach_port_deallocate(mach_task_self(), master_port);

  return SUCCESS;
}


//
// teardown_serial_io
//
void teardown_serial_io () {
  IOObjectRelease(serial_arrivals_iterator);
  IOObjectRelease(serial_departures_iterator);

  IONotificationPortDestroy(serial_notification_port);
}
