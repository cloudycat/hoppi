;;;
;;; Copyright 2012 - 2017 Paul Onions
;;;
;;; Licence: MIT, see LICENCE file for details.
;;;
(in-package :hoppi-kermit)

(defclass kermit-workspace ()
  ((folder-path      :initarg :path :accessor current-folder-path)
   (overwrite-action :initform nil  :accessor overwrite-action)
   (transfer-type    :initform nil  :accessor transfer-type)
   (str-enc-prefix   :initform nil  :accessor str-enc-prefix)
   (str-dec-state    :initform nil  :accessor str-dec-state)
   (ip-type          :initform nil  :accessor current-input-type)
   (ip-name          :initform nil  :accessor current-input-name)
   (ip-strm          :initform nil  :accessor current-input-stream)
   (ip-strm-ended    :initform nil  :accessor current-input-ended)
   (op-type          :initform nil  :accessor current-output-type)
   (op-name          :initform nil  :accessor current-output-name)
   (op-strm          :initform nil  :accessor current-output-stream))
  (:documentation "A Kermit protocol endpoint."))

(defmethod current-input-file-path ((ws kermit-workspace))
  (concatenate 'string (current-folder-path ws) (current-input-name ws)))

(defmethod current-output-file-path ((ws kermit-workspace))
  (concatenate 'string (current-folder-path ws) (current-output-name ws)))

(defmethod open-file-for-writing ((ws kermit-workspace) (path string) &optional transfer-type)
  "Open file for writing, returning T for success, NIL for failure.
Also return a string as secondary result: on success return the full
path of the file opened, and on failure return an error message.  The
optional TRANSFER-TYPE, if present, should be :TEXT, :TEXT-AS-STRING
or :BINARY, showing a file transfer preference.  If not present
then :TEXT or :BINARY will be chosen automatically upon first write,
according to file content."
  (close-current-output-stream ws)
  (let* ((full-path (namestring-merge path (current-folder-path ws)))
         (folder-part (namestring-folder-part full-path))
         (file-part (namestring-file-part full-path)))
    (setf (transfer-type ws) transfer-type)
    (setf (str-dec-state ws) :removing-header)
    (setf (current-output-type ws) :file)
    (setf (current-output-name ws) file-part)
    (setf (current-folder-path ws) folder-part)
    (setf (current-output-stream ws) nil)
    (values t (current-output-file-path ws))))

(defmethod open-message-for-writing ((ws kermit-workspace) (title string) &optional transfer-type)
  "Open a message for writing.
Return T and the title -- same return types as OPEN-FILE-FOR-WRITING.
The optional TRANSFER-TYPE, if present, should be :TEXT, :TEXT-AS-STRING
or :BINARY, showing a file transfer preference.  If not present then
:TEXT or :BINARY will be chosen automatically upon first write, according
to message content."
  (close-current-output-stream ws)
  (setf (transfer-type ws) transfer-type)
  (setf (str-dec-state ws) :removing-header)
  (setf (current-output-type ws) :message)
  (setf (current-output-name ws) title)
  (setf (current-output-stream ws) (ccl:make-vector-output-stream))
  (values t title))

(defmethod deferred-open-file-for-writing ((ws kermit-workspace))
  "Open file for writing, returning T for success, NIL for failure.
Also return a string as secondary result: on success return the full
path of the file opened (which may be different to that requested),
and on failure return an error message."
  (let* ((full-path (current-output-file-path ws))
         (strm (handler-case
                   (if (eql (transfer-type ws) :binary)
                       (open (namestring-escape full-path)
                             :direction :output
                             :if-exists (overwrite-action ws)
                             :element-type '(unsigned-byte 8))
                       (open (namestring-escape full-path)
                             :direction :output
                             :if-exists (overwrite-action ws)
                             :element-type 'character
                             :external-format :utf-8))
                 (t () nil))))
    (cond (strm
           (setf (current-output-stream ws) strm)
           (values t full-path))
          ((null (overwrite-action ws))
           (values nil (format nil "Cannot overwrite file: ~A" full-path)))
          (t
           (values nil (format nil "Cannot open file: ~A" full-path))))))

(defmethod write-to-current-output-stream ((ws kermit-workspace) (bytes vector))
  "Write the given vector of byte values to the current output stream.
Return T for success, NIL for failure.  Also return a string as secondary
result: on success return the path of the file to which we are writing,
or an empty string if we are writing a message, and on failure return an
error message."
  ;; On first write, determine transfer type and call deferred open routine
  (unless (transfer-type ws)
    (cond ((and (>= (length bytes) 6)
                (or (equalp (subseq bytes 0 6) (map 'vector #'char-code "HPHP48"))
                    (equalp (subseq bytes 0 6) (map 'vector #'char-code "HPHP49"))))
           (setf (transfer-type ws) :binary))
          (t
           (setf (transfer-type ws) :text))))
  (unless (current-output-stream ws)
    (if (eql (current-output-type ws) :file)
        (multiple-value-bind (success msg)
            (deferred-open-file-for-writing ws)
          (unless success
            (return-from write-to-current-output-stream
              (values nil msg))))
        (setf (current-output-stream ws)
              (ccl:make-vector-output-stream))))
  ;; Write output, taking note of transfer type
  (handler-case
      (progn
        (cond ((eql (transfer-type ws) :text)
               (write-sequence (map 'string #'hp-code-to-unicode-char bytes)
                               (current-output-stream ws)))
              ((eql (transfer-type ws) :text-as-string)
               (write-sequence (map 'string #'hp-code-to-unicode-char
                                    (decode-contents-of-string ws bytes))
                               (current-output-stream ws)))
              (t
               (write-sequence bytes (current-output-stream ws))))
        (values t (current-output-file-path ws)))
    (t () (values nil (format nil "Problem writing to file: ~A"
                              (current-output-file-path ws))))))

(defmethod close-current-output-stream ((ws kermit-workspace))
  "Close the current output stream."
  (when (current-output-stream ws)
    (close (current-output-stream ws))
    (setf (current-output-type ws) nil)
    (setf (current-output-name ws) nil)
    (setf (current-output-stream ws) nil)))

(defun decode-contents-of-string (ws bytes)
  "Return BYTES array with backslashes and double-quotes unescaped."
  (let ((decoded nil))
    (loop :for b :across bytes
          :do (cond ((eql (str-dec-state ws) :removing-header)
                     (when (eql b (char-code #\newline))
                       (setf (str-dec-state ws) :decoding)))
                    ((eql (str-dec-state ws) :decoding)
                     (cond ((eql b (char-code #\\))
                            (setf (str-dec-state ws) :decoding-escaped))
                           (t
                            (unless (eql b (char-code #\"))
                              (setf decoded (cons b decoded))))))
                    ((eql (str-dec-state ws) :decoding-escaped)
                     (cond ((eql b (char-code #\\))
                            (setf decoded (cons b decoded))
                            (setf (str-dec-state ws) :decoding))
                           ((eql b (char-code #\"))
                            (setf decoded (cons b decoded))
                            (setf (str-dec-state ws) :decoding))
                           (t
                            (setf decoded (cons b (cons (char-code #\\) decoded)))
                            (setf (str-dec-state ws) :decoding))))))
    (coerce (reverse decoded) 'vector)))

(defmethod open-file-for-reading ((ws kermit-workspace) (path string) &optional transfer-type)
  "Open file for reading, returning T for success, NIL for failure.
Also return a string as secondary result: on success return the full
path of the file opened, and on failure return an error message.  The
optional TRANSFER-TYPE, if present, should be :TEXT or :TEXT-AS-STRING
or :BINARY showing a file transfer preference.  If not present
then :TEXT or :BINARY will be chosen automatically according to file
type."
  (close-current-input-stream ws)
  (let* ((full-path (namestring-probe-file (namestring-merge path (current-folder-path ws))))
         (folder-part (namestring-folder-part full-path))
         (file-part (namestring-file-part full-path))
         (errmsg "")
         (file-type
          (handler-case
              ;; Determine file type, :binary or :text, by looking at first few bytes
              (with-open-file (strm (namestring-escape full-path)
                                    :direction :input
                                    :if-does-not-exist nil
                                    :element-type '(unsigned-byte 8))
                (let ((hdr (make-array 6 :element-type '(unsigned-byte 8))))
                  (read-sequence hdr strm)
                  (cond ((or (equalp hdr (map 'vector #'char-code "HPHP48"))
                             (equalp hdr (map 'vector #'char-code "HPHP49")))
                         :binary)
                        (t
                         :text))))
            (t () :text))))
    ;; Determine actual transfer type: :binary, :text or :text-as-string
    (cond ((and (eql file-type :text) (or (eql transfer-type :text)
                                          (eql transfer-type nil)))
           (setf (transfer-type ws) :text)
           (setf (str-enc-prefix ws) nil))
          ((and (eql file-type :text) (eql transfer-type :text-as-string))
           (setf (transfer-type ws) :text-as-string)
           (setf (str-enc-prefix ws) (format nil "%%HP: T(0)A(R)F(.);~%\"")))
          ((and (eql file-type :binary) (or (eql transfer-type :binary)
                                            (eql transfer-type nil)))
           (setf (transfer-type ws) :binary)
           (setf (str-enc-prefix ws) nil))
          ((and (eql file-type :binary) (or (eql transfer-type :text)
                                            (eql transfer-type :text-as-string)))
           (setf (transfer-type ws) nil)
           (setf (str-enc-prefix ws) nil)
           (setf errmsg "Cannot send BINARY file as text or string"))
          (t
           (setf (transfer-type ws) nil)
           (setf (str-enc-prefix ws) nil)
           (setf errmsg (format nil "Cannot open file: ~A" full-path))))
    ;; Open file for real now, using appropriate options
    (let ((strm (handler-case
                    (cond ((or (eql (transfer-type ws) :text)
                               (eql (transfer-type ws) :text-as-string))
                           (open (namestring-escape full-path)
                                 :direction :input
                                 :if-does-not-exist nil
                                 :element-type 'character
                                 :external-format :utf-8))
                          ((eql (transfer-type ws) :binary)
                           (open (namestring-escape full-path)
                                 :direction :input
                                 :if-does-not-exist nil
                                 :element-type '(unsigned-byte 8)))
                          (t
                           nil))
                  (t () nil))))
      (cond (strm
             (setf (current-input-type ws) :file)
             (setf (current-input-name ws) file-part)
             (setf (current-folder-path ws) folder-part)
             (setf (current-input-stream ws) strm)
             (values t full-path))
            (t
             (values nil errmsg))))))

(defmethod open-message-for-reading ((ws kermit-workspace) (title string) (msg string) &optional (transfer-type :TEXT))
  "Open a message for reading.
The optional TRANSFER-TYPE, if present, should be :TEXT or :TEXT-AS-STRING
showing a file transfer preference."
  (close-current-input-stream ws)
  (setf (transfer-type ws) transfer-type)
  (setf (current-input-type ws) :message)
  (setf (current-input-name ws) title)
  (setf (current-input-stream ws) (make-string-input-stream msg)))

(defmethod read-from-current-input-stream ((ws kermit-workspace) max-bytes)
  "Read up to MAX-BYTES from the current input stream.
Return a vector of byte values no longer than max-bytes, or NIL if the
end of the file has been reached."
  (let* ((n (max 0 (- max-bytes (length (str-enc-prefix ws)))))
         (seq (make-array n :element-type (if (eql (transfer-type ws) :binary)
                                              '(unsigned-byte 8)
                                              'character)
                            :fill-pointer t))
         (num-items-read (handler-case
                             (read-sequence seq (current-input-stream ws))
                           (t () 0))))
    (setf (fill-pointer seq) num-items-read)
    (when (eql (transfer-type ws) :text-as-string)
      (setf seq (encode-as-contents-of-string seq))
      (when (str-enc-prefix ws)
        (setf seq (concatenate 'string (str-enc-prefix ws) seq)))
      (when (and (< num-items-read n) (not (current-input-ended ws)))
        (setf seq (concatenate 'string seq "\""))))
    (let ((items-to-send (if (<= (length seq) max-bytes)
                             seq
                             (subseq seq 0 max-bytes)))
          (items-deferred (if (<= (length seq) max-bytes)
                             nil
                             (subseq seq max-bytes))))
      (setf (str-enc-prefix ws) items-deferred)
      (when (< num-items-read n)
        (setf (current-input-ended ws) t))
      (if (eql (transfer-type ws) :binary)
          items-to-send
          (map 'vector #'unicode-char-to-hp-code items-to-send)))))

(defmethod eof-current-input-stream-p ((ws kermit-workspace))
  "Return T if end of file has been reached, otherwise NIL."
  (and (null (str-enc-prefix ws))
       (current-input-ended ws)))

(defmethod close-current-input-stream ((ws kermit-workspace))
  "Close the current input stream."
  (when (current-input-stream ws)
    (close (current-input-stream ws))
    (setf (current-input-type ws) nil)
    (setf (current-input-name ws) nil)
    (setf (current-input-stream ws) nil)
    (setf (current-input-ended ws) nil)))

(defun encode-as-contents-of-string (str)
  "Return string STR with backslashes and double-quotes escaped."
  (apply #'concatenate
         (cons 'string
               (map 'list
                    (lambda (c)
                      (cond ((eql c #\") "\\\"")
                            ((eql c #\\) "\\\\")
                            (t           (string c))))
                    str))))

(defmethod update-current-folder-path ((ws kermit-workspace) requested-path)
  "Update the current folder path according to that requested.
Return T for success, NIL for failure.  Also return a string as
secondary value, either the name of the path when successful, or an
error message when failed."
  (let* ((new-path (parse-path ws requested-path))
         (true-path (namestring-probe-file new-path)))
    (if true-path
        (progn
          (setf (current-folder-path ws) true-path)
          (values t true-path))
        (values nil (format nil "Invalid directory: ~A" new-path)))))

(defun parse-path (workspace requested-path)
  "Parse REQUESTED-PATH and return the actual path to use."
  (typecase requested-path
    (null (current-folder-path workspace))
    (string (progn
              ;; strip any enclosing [ ]
              (when (and (eql (char requested-path 0) #\[)
                         (eql (char requested-path (1- (length requested-path))) #\]))
                (setf requested-path (subseq requested-path 1 (1- (length requested-path)))))
              ;; interpret path string as referring to a directory
              ;; (even if it is missing a trailing '/')
              (let ((path (undosify-path requested-path)))
                (namestring-ensure-folder
                 (if (eql (char path 0) #\/)
                     path ; absolute path
                     (namestring-merge path (current-folder-path workspace))))))))) ; relative path

(defun dosify-path (path)
  "Return a string representing PATH in a DOS-like format."
  (let ((drive (if (eql (char path 0) #\/) "H:" "")))
    (concatenate 'string drive (substitute #\\ #\/ path))))

(defun undosify-path (dos-path)
  "Unmangle a `dosified' path string, returning a string."
  (let ((start (if (and (>= (length dos-path) 2)
                        (eql (char dos-path 1) #\:))
                   2
                   0)))
    (subseq (substitute #\/ #\\ dos-path) start)))

(defmethod list-folder-for-reading ((ws kermit-workspace) path &key (style :RPL))
  "List folder for reading, returning a string listing for success, NIL for failure.
If STYLE is :DOS then list folder enough like 'DIR /W' on DOS so that an HP48
can interpret it.  Also return the path of the folder listed as secondary result."
  (let ((chosen-path (parse-path ws path)))
    (values (handler-case
                (let* ((dirs (namestring-list-directory chosen-path :files nil :directories t))
                       (dirnames (remove-hidden-names dirs))
                       (files (namestring-list-directory chosen-path :files t :directories nil))
                       (filenames (remove-hidden-names files)))
                  (dbg-msg "~&DIRNAMES: ~S" dirnames)
                  (dbg-msg "~&FILENAMES: ~S" filenames)
                  (case style
                    ((:DOS)
                     (format nil "Directory of ~A~%[..] ~{[~A]  ~}~{~A  ~}"
                             (dosify-path chosen-path)
                             (mapcar #'namestring-ensure-file ; remove trailing /
                                     dirnames)
                             filenames))
                    ((:RPL)
                     (format nil "~S {~{~S ~}} {~{~S ~}}"
                             chosen-path
                             (mapcar #'namestring-ensure-file ; remove trailing /
                                     dirnames)
                             filenames))))
              (t () nil))
            chosen-path)))

(defun remove-hidden-names (paths)
  (remove-if (lambda (str)
	       (or (zerop (length str))
		   (eql (char str 0) #\.)))
	     paths))
