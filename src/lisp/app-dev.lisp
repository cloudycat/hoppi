;;;
;;; Copyright 2012 - 2016 Paul Onions
;;;
;;; Licence: MIT, see LICENCE file for details.
;;;
;;; To work on the Hoppi application in the CCL IDE: first ensure
;;; the Hoppi system is loaded, then `load' this file and call
;;; `hoppi:dev-app' to open the Hoppi windows in the IDE.
;;;
(in-package :hoppi)

(defparameter *this-directory* (make-pathname :directory (pathname-directory *load-truename*)))

;;; A dummy application to feed to the HOPPI-INIT function
;;;
(defclass dummy-application (ns:ns-object)
  ((show-packet-logger-widget     :foreign-type :id :accessor show-packet-logger-widget)
   (show-message-logger-widget    :foreign-type :id :accessor show-message-logger-widget)
   (receive-file-as-string-widget :foreign-type :id :accessor receive-file-as-string-widget)
   (nib-objects :accessor nib-objects :initform nil))
  (:metaclass ns:+ns-object))

;;; A function to start the Hoppi application GUI, opening main and packet logger windows
;;;
(defun dev-app ()
  "Start running Hoppi in the CCL IDE."
  (setf (ccl:current-directory) *this-directory*)
  (let ((application-object (make-instance 'dummy-application)))
    (gui::execute-in-gui
     (lambda ()
       (setf *lib-path* (namestring (truename #P"../lib/")))
       (setf *nib-path* (namestring (truename #P"../gui/")))
       (hoppi-init application-object)
       (make-instance 'packet-logger)
       (make-instance 'message-logger)
       (make-instance 'grob-viewer)
       (setf *dbg-output* *trace-output*)))))
