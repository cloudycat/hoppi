;;;
;;; Copyright 2012 - 2017 Paul Onions
;;;
;;; Licence: MIT, see LICENCE file for details.
;;;
;;; Package definitions for the Hoppi project.
;;;
(in-package :cl-user)

(defpackage :hoppi-utils
  (:use :common-lisp)
  (:export
   ;; dbg-utils.lisp
   #:dbg-msg
   ;; namestring-utils.lisp
   #:namestring-seperator-p #:namestring-components
   #:namestring-is-folder-p #:namestring-is-file-p
   #:namestring-ensure-folder #:namestring-ensure-file
   #:namestring-folder-part #:namestring-file-part
   #:namestring-escape #:namestring-unescape
   #:namestring-merge #:namestring-home-folder
   #:namestring-list-directory #:namestring-probe-file))

(defpackage :hoppi-kermit
  (:use :common-lisp :hoppi-utils)
  (:export
   ;; char-map.lisp
   #:hp-code-to-unicode-char #:unicode-char-to-hp-code
   ;; workspace.lisp
   #:kermit-workspace #:overwrite-action #:current-folder-path
   #:current-input-type #:current-input-name #:current-input-stream
   #:current-output-type #:current-output-name #:current-output-stream
   #:current-input-file-path #:current-output-file-path
   #:open-file-for-writing #:open-message-for-writing
   #:write-to-current-output-stream #:close-current-output-stream
   #:open-file-for-reading #:open-message-for-reading
   #:read-from-current-input-stream #:close-current-input-stream
   #:eof-current-input-stream-p #:list-folder-for-reading
   #:update-current-folder-path
   ;; kermit.lisp
   #:kermit-packet #:make-kermit-packet #:packet-seq #:packet-type #:packet-data+chk
   #:packet-data #:packet-checksum #:packet-rx-checksum
   #:checksum-size #:verify-checksum-p
   #:kermit-buffer #:buffer-level
   #:kermit-rx-buffer #:push-byte #:push-bytes #:pop-packet #:packet-available-p
   #:kermit-tx-buffer #:push-packet #:pop-byte #:pop-bytes #:byte-available-p
   #:kermit-protocol-controller #:retry-count #:retry-limit #:timer-count
   #:current-parameters #:pending-parameters
   #:num-user-bytes-sent #:reset-protocol
   #:protocol-state #:protocol-error-message
   #:tx-error-message #:rx-error-message
   #:tx-buffer #:rx-buffer #:workspace #:transfer-type
   #:run-protocol #:run-protocol-step
   #:one-second-tick #:send-file))

(defpackage :hoppi
  (:use :common-lisp :hoppi-utils :hoppi-kermit)
  (:export
   ;; app.lisp
   #:*lib-path* #:*nib-path* #:hoppi-application #:hoppi-init))
