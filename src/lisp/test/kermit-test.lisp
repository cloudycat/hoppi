;;;
;;; Copyright 2012 - 2017 Paul Onions
;;;
;;; Licence: MIT, see LICENCE file for details.
;;;
(in-package :hoppi-kermit)

;;; Test data encoding
;;;
;;; See tables on page 27 and 28 of the Kermit Protocol Manual, 6th edition.
;;;
(defun print-data-codepoints (kpc test-bytes)
  (format t "  b ==> x1, x8~%")
  (loop :for byte :in test-bytes
        :do (let ((x1 (encode-byte kpc byte))
                  (x8 (encode-data kpc (make-array 8 :initial-element byte))))
              (format t "~3D ==> ~S, ~S ==> ~S, ~S~%" byte x1 x8 (decode-data kpc x1) (decode-data kpc x8))
              (format t "~:[ ~;'~]~:[ ~;^~]~A"
                      (>= byte 128)
                      (< (mod byte 128) 32)
                      (if (< (mod byte 128) 32) (code-char (+ (mod byte 128) 64)) (code-char (mod byte 128))))
              (format t " ==> ")
              (loop :for b :across x1
                    :do (format t "~A" (code-char b)))
              (format t ",  ")
              (loop :for b :across x8
                    :do (format t "~A" (code-char b)))
              (format t "~%~%"))))

(defun test-encode-data ()
  (flet ((char (c) (char-code c))
         (ctrl (k) (- k 64))
         (meta (k) (+ k 128)))
    (let ((kpc (make-instance 'kermit-protocol-controller))
          (test-bytes (list (char #\A)
                            (ctrl (char #\A))
                            (meta (char #\A))
                            (meta (ctrl (char #\A)))
                            (char #\#)
                            (meta (char #\#))
                            (char #\&)
                            (meta (char #\&))
                            (char #\~)
                            (meta (char #\~))
                            0)))
      (setf (repeat-prefix-byte (current-parameters kpc)) (char-code #\~))
      (setf (repeat-prefix-enable (current-parameters kpc)) t)
      (setf (binary-quote-byte (current-parameters kpc)) (char #\&))
      ;; Without binary prefix
      (format t "*** Binary quoting disabled ***~%")
      (setf (binary-quote-enable (current-parameters kpc)) nil)
      (print-data-codepoints kpc test-bytes)
      ;; With binary prefix
      (format t "*** Binary quoting enabled ***~%")
      (setf (binary-quote-enable (current-parameters kpc)) t)
      (print-data-codepoints kpc test-bytes)
      ;; 120 NULs
      (format t "*** 120 NULs ***~%")
      (loop :for b :across (encode-data kpc (make-array 120 :initial-element 0))
            :do (format t "~A" (code-char b))))
      (format t "~%")))
      
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Dummy workspace
;;;  
(defparameter *dummy-string* "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz")

(defclass dummy-workspace ()
  ((strm :initform t :initarg :message-stream)
   (bytes-read :initform 0))
  (:documentation "A dummy Kermit workspace delegate, for testing purposes."))

(defmethod open-file-for-writing ((dw dummy-workspace) (path pathname) &optional transfer-type)
  (declare (ignore transfer-type))
  (with-slots (strm) dw
    (setf (current-output-name dw) path)
    (format strm "DW: opening output file: ~A~%" path)))

(defmethod write-to-current-output-stream ((dw dummy-workspace) (bytes vector))
  (with-slots (strm) dw
    (format strm "DW: writing...~%~A~%" (map 'string #'hp-code-to-unicode-char bytes))))

(defmethod close-current-output-stream ((dw dummy-workspace))
  (with-slots (strm) dw
    (format strm "DW: closing output file: ~A~%" (current-output-name dw))
    (setf (current-output-name dw) nil)))

(defmethod open-file-for-reading ((dw dummy-workspace) (path pathname) &optional transfer-type)
  (declare (ignore transfer-type))
  (with-slots (strm bytes-read) dw
    (setf (current-input-name dw) path)
    (setf bytes-read 0)
    (format strm "DW: opening input file: ~A~%" path)))

(defmethod read-from-current-input-stream ((dw dummy-workspace) max-bytes)
  (with-slots (strm bytes-read) dw
    (let* ((num-bytes-left (- 100 bytes-read))
           (num-bytes-to-return (min num-bytes-left max-bytes)))
      (incf bytes-read num-bytes-to-return)
      (format strm "DW: reading: ~A~%" (map 'vector #'unicode-char-to-hp-code (subseq *dummy-string* 0 num-bytes-to-return))))))

(defmethod eof-current-input-stream-p ((dw dummy-workspace))
  (with-slots (bytes-read) dw
    (>= bytes-read 100)))

(defmethod close-current-input-stream ((dw dummy-workspace))
  (with-slots (strm) dw
    (format strm "DW: closing input file: ~A~%" (current-input-name dw))
    (setf (current-input-name dw) nil)))

