;;;
;;; Copyright 2012 - 2019 Paul Onions
;;;
;;; Licence: MIT, see LICENCE file for details.
;;;
;;; To build the Hoppi application: first ensure the Hoppi system is loaded,
;;; then `load' this file into the CCL IDE and call `hoppi::build-app'.
;;;
;;; This will create a standalone application, Hoppi.app, in the folder
;;; containing the repository top level.
;;;
(in-package :hoppi)

(require :build-application)

(defparameter *this-directory* (make-pathname :directory (pathname-directory *load-truename*)))

(defun build-app ()
  "Build the Hoppi application."
  (setf (ccl:current-directory) *this-directory*)
  (let ((lib-path (truename (concatenate 'string (namestring *this-directory*) "../lib/")))
        (nib-path (truename (concatenate 'string (namestring *this-directory*) "../gui/")))
        (app-path (truename (concatenate 'string (namestring *this-directory*) "../../../"))))
    (multiple-value-bind (second minute hour date month year) (get-decoded-time)
      (declare (ignore second))
      (let ((build-info (format nil "~D/~D/~D ~2,'0D:~2,'0D"
                                year month date hour minute))
            (app-pathname (merge-pathnames "Hoppi.app/" app-path)))
        (format t "~&Building ~A~%" app-pathname)
        (when (probe-file app-pathname)
          (ccl::delete-directory app-pathname))
        (asdf:clear-configuration)
	(setf *this-directory* nil)
        (setf #+CCL-1.11 gui::*delegate-class-name*
              #-CCL-1.11 gui::*default-ns-application-proxy-class-name*
              "HoppiApplicationDelegate")
        (ccl::build-application :name "Hoppi"
                                :directory app-path
                                :nibfiles (list (merge-pathnames #P"hoppi-main-menu.nib" nib-path)
                                                (merge-pathnames #P"hoppi-controller.nib" nib-path)
                                                (merge-pathnames #P"packet-logger.nib" nib-path)
                                                (merge-pathnames #P"message-logger.nib" nib-path)
                                                (merge-pathnames #P"grob-viewer.nib" nib-path)
                                                (merge-pathnames #P"Hoppi.icns" nib-path)
                                                (merge-pathnames #P"Credits.rtf" nib-path)
                                                (merge-pathnames #P"hoppi-help/" nib-path))
                                :private-frameworks (list (merge-pathnames #P"hoppi-io" lib-path))
                                :copy-ide-resources nil
                                :info-plist (ccl::make-info-dict :principal-class "HoppiApplication"
                                                                 :bundle-identifier "com.cloudycat.Hoppi"
                                                                 :icon-file "Hoppi.icns"
                                                                 :help-book-folder "hoppi-help"
                                                                 :help-book-name "Hoppi Help"
                                                                 :getinfo-string "Copyright 2013 - 2019 Paul Onions"
                                                                 :version build-info
                                                                 :short-version-string "1.4")
                                :main-nib-name nil)))))
