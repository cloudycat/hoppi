;;;
;;; Copyright 2012 - 2016 Paul Onions
;;;
;;; Licence: MIT, see LICENCE file for details.
;;;
;;; System definition for the Hoppi application.
;;;
(in-package :cl-user)

(asdf:defsystem "hoppi"
  :description "Hoppi: a Kermit communication program for HP48/49/50 calculators on MacOSX."
  :author "Paul Onions <paul.onions@acm.org>"
  :depends-on ("hoppi-core")
  :components ((:file "app"))
  :serial t)
