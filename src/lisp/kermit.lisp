;;;
;;; Copyright 2012 - 2017 Paul Onions
;;;
;;; Licence: MIT, see LICENCE file for details.
;;;
(in-package :hoppi-kermit)

(defparameter +min-packet-size+ 2)
(defparameter +max-packet-size+ 94)

(defparameter +soh+ 1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Kermit data processing
;;;
(defun encode-tochar (byte)
  "Kermit specified tochar() encoding function."
  (check-type byte (integer 0 255))
  (mod (+ byte 32) 256))

(defun decode-unchar (byte)
  "Kermit specified unchar() decoding function."
  (check-type byte (integer 0 255))
  (mod (- byte 32) 256))

(defun code-ctl (byte)
  "Kermit specified ctl() encoding/decoding function."
  (check-type byte (integer 0 255))
  (logxor byte 64))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Kermit packets
;;;
(defclass kermit-packet ()
  ((seq               :initarg :seq               :reader packet-seq)
   (type              :initarg :type              :reader packet-type)
   (data+chk          :initarg :data+chk          :reader packet-data+chk))
  (:documentation "Kermit packet class."))

(defmethod print-object ((pkt kermit-packet) strm)
  (print-unreadable-object (pkt strm :type t :identity t)
    (with-slots (seq type data+chk) pkt
      (format strm "~A ~C \"~{~A~}\""
              seq
              (code-char type)
              (loop :for b :across data+chk :collect (hp-code-to-unicode-char b))))))

(defmethod packet-data ((pkt kermit-packet) checksum-size)
  (let ((data+chk (packet-data+chk pkt)))
    (subseq data+chk 0 (- (length data+chk) checksum-size))))

(defmethod packet-checksum ((pkt kermit-packet) checksum-size)
  (let ((data+chk (packet-data+chk pkt)))
    (subseq data+chk (- (length data+chk) checksum-size))))

(defun compute-1-byte-checksum (seq type data)
  (let* ((len (+ (length data) 3))
         (acc (+ (encode-tochar len) (encode-tochar seq) type)))
    (loop :for b :across data :do (incf acc b))
    (vector (encode-tochar (logand (+ acc (ash (logand acc 192) -6)) 63)))))

(defun compute-2-byte-checksum (seq type data)
  (let* ((len (+ (length data) 4))
         (acc (+ (encode-tochar len) (encode-tochar seq) type)))
    (loop :for b :across data :do (incf acc b))
    (vector (encode-tochar (logand (ash acc -6) 63))
            (encode-tochar (logand acc 63)))))

(defun compute-3-byte-checksum (seq type data)
  (let* ((len (+ (length data) 5))
         (crc-data (make-array (max 0 (- len 2)) :element-type 'fixnum)))
    ;; prepare data to be CRC'd into a single vector
    (setf (aref crc-data 0) (encode-tochar len))
    (setf (aref crc-data 1) (encode-tochar seq))
    (setf (aref crc-data 2) type)
    (loop :for b :across data
          :for n :from 3
          :do (setf (aref crc-data n) b))
    ;; apply CRC algorithm as described in the Kermit Protocol Manual, 6th ed.
    (let ((crc 0)
          (q 0))
      (loop :for c :across crc-data
            :do (progn (setf q (logand (logxor crc c) 15))
                       (setf crc (logxor (ash crc -4) (* q 4225)))
                       (setf q (logand (logxor crc (ash c -4)) 15))
                       (setf crc (logxor (ash crc -4) (* q 4225)))))
      (vector (encode-tochar (logand (ash crc -12) 15))
              (encode-tochar (logand (ash crc -6) 63))
              (encode-tochar (logand crc 63))))))

(defun compute-checksum (seq type data checksum-size)
  (case checksum-size
    ((0) (vector))  ; an empty vector
    ((1) (compute-1-byte-checksum seq type data))
    ((2) (compute-2-byte-checksum seq type data))
    ((3) (compute-3-byte-checksum seq type data))))

(defmethod verify-checksum-p ((pkt kermit-packet) checksum-size)
  (let ((seq (packet-seq pkt))
        (type (packet-type pkt))
        (data (packet-data pkt checksum-size))
        (checksum (packet-checksum pkt checksum-size)))
    (equalp checksum (compute-checksum seq type data checksum-size))))

(defun make-kermit-packet (seq type &key data+chk (data #()) (checksum-size 0))
  "Make a Kermit packet.
If data+chk is given then use it, otherwise take data as given and append
to it a checksum computed according to checksum-size."
  (let ((type (if (characterp type) (char-code type) type)))
    (make-instance 'kermit-packet :seq seq
                                  :type type
                                  :data+chk (if data+chk
                                                data+chk
                                                (concatenate 'vector
                                                             data
                                                             (compute-checksum seq type data checksum-size))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Kermit buffer -- base class for Kermit transmit and receive buffers
;;;
(defclass kermit-buffer ()
  ((buf    :initform (make-array 1024 :element-type 'fixnum))
   (rd-ptr :initform 0)
   (wr-ptr :initform 0)
   (protocol-controller :initarg :protocol-controller :accessor protocol-controller))
  (:documentation "A kermit buffer base class."))

(defmethod rd-incf ((kbuf kermit-buffer) &optional (increment 1))
  (with-slots (buf rd-ptr) kbuf
    (setf rd-ptr (mod (+ rd-ptr increment) (length buf)))))

(defmethod wr-incf ((kbuf kermit-buffer) &optional (increment 1))
  (with-slots (buf wr-ptr) kbuf
    (setf wr-ptr (mod (+ wr-ptr increment) (length buf)))))

(defmethod rd-peek ((kbuf kermit-buffer) &optional (offset 0))
  (with-slots (buf rd-ptr) kbuf
    (aref buf (mod (+ rd-ptr offset) (length buf)))))

(defmethod rd-pop ((kbuf kermit-buffer) &optional length)
  "Pop byte(s) from buffer.
If length is specified then return a vector of the appropriate length,
otherwise return a single byte (not a vector)."
  (if (null length)
      (let ((datum (rd-peek kbuf)))
        (rd-incf kbuf)
        datum)
      (let ((data (make-array length :element-type 'fixnum)))
        (loop :for n :from 0 :to (1- (length data))
              :do (setf (aref data n) (rd-pop kbuf)))
        data)))

(defmethod wr-push ((kbuf kermit-buffer) byte)
  "Push the given byte into the buffer."
  (with-slots (buf wr-ptr) kbuf
    (setf (aref buf wr-ptr) byte)
    (wr-incf kbuf)))

(defmethod buffer-level ((kbuf kermit-buffer))
  "Return the number of bytes in the buffer."
  (with-slots (buf rd-ptr wr-ptr) kbuf
    ;; Always assume the wr-ptr is ahead of the rd-ptr, accounting for modulo wrap
    (if (>= wr-ptr rd-ptr)
        (- wr-ptr rd-ptr)
        (- (+ wr-ptr (length buf)) rd-ptr))))

(defmethod reset-buffer ((kbuf kermit-buffer))
  "Forcibly empty the buffer."
  (with-slots (rd-ptr wr-ptr) kbuf
    (setf rd-ptr 0)
    (setf wr-ptr 0)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Kermit receive buffer -- bytes in, packets out
;;;
(defclass kermit-rx-buffer (kermit-buffer)
  ()
  (:documentation "A kermit receive buffer: push bytes in, pop packets out."))

(defmethod find-start-of-packet ((kbuf kermit-rx-buffer))
  "Find the start of a packet in the given receive buffer.
Set rd-ptr to start of packet, if possible, and return T.
Otherwise return NIL."
  (with-slots (buf rd-ptr wr-ptr) kbuf
    (loop :while (and (/= (aref buf rd-ptr) +soh+) (/= rd-ptr wr-ptr))
          :do (rd-incf kbuf))
    (= (rd-peek kbuf) +soh+)))

(defmethod packet-available-p ((kbuf kermit-rx-buffer))
  (with-slots (buf rd-ptr wr-ptr) kbuf
    (and (find-start-of-packet kbuf)
         (let ((buf-lvl (buffer-level kbuf)))
           (and (>= buf-lvl 2)
                (let ((pkt-len (min +max-packet-size+ (max +min-packet-size+ (decode-unchar (rd-peek kbuf 1))))))
                  (>= buf-lvl (+ pkt-len 2))))))))

(defmethod push-byte ((kbuf kermit-rx-buffer) byte)
  "Push the given byte into the kermit-rx-buffer."
  (declare (inline wr-push))
  (wr-push kbuf byte))

(defmethod push-bytes ((kbuf kermit-rx-buffer) byte/s)
  "Push the given byte, or vector of bytes, into the kermit-rx-buffer.
Return the number of bytes pushed."
  (if (numberp byte/s)
      (progn
        (wr-push kbuf byte/s)
        1)
      (progn
        (loop :for b :across byte/s
              :do (wr-push kbuf b))
        (length byte/s))))

(defmethod pop-packet ((kbuf kermit-rx-buffer))
  "Pop a packet from the kermit-rx-buffer.
Return packet if one is available, else return NIL."
  (when (packet-available-p kbuf)
    (let* ((pkt-len (min +max-packet-size+ (max +min-packet-size+ (decode-unchar (rd-peek kbuf 1)))))
           (seq (decode-unchar (rd-peek kbuf 2)))
           (type (rd-peek kbuf 3))
           (data+chk (let ((data (make-array (max 0 (- pkt-len 2)) :element-type 'fixnum)))
                       (loop :for b :from 4 :to (- (+ pkt-len 2) 1)
                             :for n :from 0
                             :do (setf (aref data n) (rd-peek kbuf b)))
                       data))
           (pkt (make-kermit-packet seq type :data+chk data+chk)))
      (rd-incf kbuf (+ pkt-len 2))
      pkt)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Kermit transmit buffer -- packets in, bytes out
;;;
(defclass kermit-tx-buffer (kermit-buffer)
  ()
  (:documentation "A kermit transmit buffer: push packets in, pop bytes out."))

(defmethod push-packet ((kbuf kermit-tx-buffer) pkt)
  (let ((seq (packet-seq pkt))
        (type (packet-type pkt))
        (data+chk (packet-data+chk pkt))
        (num-pad-bytes (tx-num-pad-bytes (current-parameters (protocol-controller kbuf))))
        (pad-byte (tx-pad-byte (current-parameters (protocol-controller kbuf))))
        (terminator-byte (tx-packet-terminator-byte (current-parameters (protocol-controller kbuf)))))
    (loop :repeat num-pad-bytes
          :do (wr-push kbuf pad-byte))
    (wr-push kbuf +soh+)
    (wr-push kbuf (encode-tochar (+ (length data+chk) 2)))
    (wr-push kbuf (encode-tochar seq))
    (wr-push kbuf type)
    (loop :for b :across data+chk
          :do (wr-push kbuf b))
    (wr-push kbuf terminator-byte)))

(defmethod byte-available-p ((kbuf kermit-tx-buffer))
  "Return T if the buffer is non-empty, otherwise NIL."
  (> (buffer-level kbuf) 0))

(defmethod pop-byte ((kbuf kermit-tx-buffer))
  "Return a single byte popped from the given kermit transmit buffer."
  (rd-pop kbuf))

(defmethod pop-bytes ((kbuf kermit-tx-buffer) length)
  "Return a vector of bytes popped from the given kermit transmit buffer."
  (rd-pop kbuf length))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Kermit protocol parameters
;;;
(defclass kermit-protocol-parameters ()
  ((calculator-timeout-period :accessor calculator-timeout-period)
   (hoppi-timeout-period      :accessor hoppi-timeout-period)
   (checksum-size             :accessor checksum-size)
   (binary-quote-enable       :accessor binary-quote-enable)
   (repeat-prefix-enable      :accessor repeat-prefix-enable)
   (binary-quote-byte         :accessor binary-quote-byte)
   (repeat-prefix-byte        :accessor repeat-prefix-byte)
   (tx-max-packet-length      :accessor tx-max-packet-length)
   (tx-num-pad-bytes          :accessor tx-num-pad-bytes)
   (tx-pad-byte               :accessor tx-pad-byte)
   (tx-packet-terminator-byte :accessor tx-packet-terminator-byte)
   (tx-ctrl-quote-byte        :accessor tx-ctrl-quote-byte)
   (rx-ctrl-quote-byte        :accessor rx-ctrl-quote-byte))
  (:documentation "Communication parameters for the Kermit file transfer protocol."))

(defmethod set-default-params ((kpp kermit-protocol-parameters))
  "Set the Kermit protocol parameters to their default values."
  (setf (calculator-timeout-period kpp) 3)
  (setf (hoppi-timeout-period kpp)      20)
  (setf (checksum-size kpp)             1)
  (setf (binary-quote-enable kpp)       nil)
  (setf (repeat-prefix-enable kpp)      nil)
  (setf (binary-quote-byte kpp)         (char-code #\Y))
  (setf (repeat-prefix-byte kpp)        (char-code #\Space))
  (setf (tx-max-packet-length kpp)      80)
  (setf (tx-num-pad-bytes kpp)          0)
  (setf (tx-pad-byte kpp)               0)
  (setf (tx-packet-terminator-byte kpp) 13)
  (setf (tx-ctrl-quote-byte kpp)        (char-code #\#))
  (setf (rx-ctrl-quote-byte kpp)        (char-code #\#)))

(defmethod initialize-instance :after ((kpp kermit-protocol-parameters) &key &allow-other-keys)
  (set-default-params kpp))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Kermit protocol controller
;;;
(defclass kermit-protocol-controller ()
  ((protocol-state                :accessor protocol-state)
   (protocol-error-message        :accessor protocol-error-message)
   (seq-num                       :accessor seq-num)
   (retry-count                   :accessor retry-count)
   (retry-limit                   :accessor retry-limit)
   (timer-count                   :accessor timer-count)
   (send-idle-nak                 :accessor send-idle-nak :initform nil)
   (honour-timeout-period-request :accessor honour-timeout-period-request)
   (current-parameters            :accessor current-parameters)
   (pending-parameters            :accessor pending-parameters)
   (pending-tx-chunks             :accessor pending-tx-chunks)
   (num-user-bytes-sent           :accessor num-user-bytes-sent)
   (tx-error-message              :accessor tx-error-message)
   (rx-error-message              :accessor rx-error-message)
   (tx-buffer                     :accessor tx-buffer :initarg :tx-buffer)
   (rx-buffer                     :accessor rx-buffer :initarg :rx-buffer)
   (workspace                     :accessor workspace :initarg :workspace)
   (transfer-type                 :accessor transfer-type :initform nil))
  (:documentation "An endpoint for the Kermit file transfer protocol."))

(defmethod set-default-state ((kpc kermit-protocol-controller))
  (set-default-params (current-parameters kpc))
  (set-default-params (pending-parameters kpc))
  (setf (protocol-state kpc)                :receive-server-idle)
  (setf (protocol-error-message kpc)        "")
  (setf (seq-num kpc)                       0)
  (setf (retry-count kpc)                   0)
  (setf (retry-limit kpc)                   5)
  (setf (timer-count kpc)                   0)
  (setf (honour-timeout-period-request kpc) nil)
  (setf (pending-tx-chunks kpc)             nil)
  (setf (num-user-bytes-sent kpc)           0)
  (setf (tx-error-message kpc)              "")
  (setf (rx-error-message kpc)              "")
  (setf (transfer-type kpc)                 nil))

(defmethod initialize-instance :after ((kpc kermit-protocol-controller) &key &allow-other-keys)
  (setf (current-parameters kpc) (make-instance 'kermit-protocol-parameters))
  (setf (pending-parameters kpc) (make-instance 'kermit-protocol-parameters))
  (set-default-state kpc))

(defmethod reset-protocol ((kpc kermit-protocol-controller))
  (reset-buffer (tx-buffer kpc))
  (reset-buffer (rx-buffer kpc))
  (set-default-state kpc))

(defmethod activate-pending-parameters ((kpc kermit-protocol-controller))
  (let ((pending (pending-parameters kpc))
        (current (current-parameters kpc)))
    (setf (calculator-timeout-period current) (calculator-timeout-period pending))
    (setf (hoppi-timeout-period current)      (hoppi-timeout-period pending))
    (setf (checksum-size current)             (checksum-size pending))
    (setf (binary-quote-enable current)       (binary-quote-enable pending))
    (setf (repeat-prefix-enable current)      (repeat-prefix-enable pending))
    (setf (binary-quote-byte current)         (binary-quote-byte pending))
    (setf (repeat-prefix-byte current)        (repeat-prefix-byte pending))
    (setf (tx-max-packet-length current)      (tx-max-packet-length pending))
    (setf (tx-num-pad-bytes current)          (tx-num-pad-bytes pending))
    (setf (tx-pad-byte current)               (tx-pad-byte pending))
    (setf (tx-packet-terminator-byte current) (tx-packet-terminator-byte pending))
    (setf (tx-ctrl-quote-byte current)        (tx-ctrl-quote-byte pending))
    (setf (rx-ctrl-quote-byte current)        (rx-ctrl-quote-byte pending))))

;;;
;;; Data encoding and decoding routines
;;;
(defmethod encode-byte ((kpc kermit-protocol-controller) byte)
  "Encode a byte value for transmission.
Taking care to perform control and binary prefixing.  Return a vector
of byte values."
  (check-type byte (integer 0 255))
  (with-accessors ((tx-ctrl-quote-byte   tx-ctrl-quote-byte)
                   (binary-quote-enable  binary-quote-enable)
                   (binary-quote-byte    binary-quote-byte)
                   (repeat-prefix-enable repeat-prefix-enable)
                   (repeat-prefix-byte   repeat-prefix-byte))
      (current-parameters kpc)
    (cond ((or (< byte 32) (= byte 127))
           (vector tx-ctrl-quote-byte (code-ctl byte)))
          ((< byte 128)
           (cond ((eql byte tx-ctrl-quote-byte)
                  (vector tx-ctrl-quote-byte tx-ctrl-quote-byte))
                 ((and binary-quote-enable (eql byte binary-quote-byte))
                  (vector tx-ctrl-quote-byte binary-quote-byte))
                 ((and repeat-prefix-enable (eql byte repeat-prefix-byte))
                  (vector tx-ctrl-quote-byte repeat-prefix-byte))
                 (t
                  (vector byte))))
          (binary-quote-enable
           (if (or (< byte 160) (= byte 255))
               (vector binary-quote-byte tx-ctrl-quote-byte (code-ctl (mod byte 128)))
               (cond ((eql (mod byte 128) tx-ctrl-quote-byte)
                      (vector binary-quote-byte tx-ctrl-quote-byte tx-ctrl-quote-byte))
                     ((eql (mod byte 128) binary-quote-byte)
                      (vector binary-quote-byte tx-ctrl-quote-byte binary-quote-byte))
                     ((and repeat-prefix-enable (eql (mod byte 128) repeat-prefix-byte))
                      (vector binary-quote-byte tx-ctrl-quote-byte repeat-prefix-byte))
                     (t
                      (vector binary-quote-byte (mod byte 128))))))
          (t                            ; no binary quoting
           (if (or (< byte 160) (= byte 255))
               (vector tx-ctrl-quote-byte (code-ctl byte))
               (cond ((eql (mod byte 128) tx-ctrl-quote-byte)
                      (vector tx-ctrl-quote-byte byte))
                     ((and repeat-prefix-enable (eql (mod byte 128) repeat-prefix-byte))
                      (vector tx-ctrl-quote-byte byte))
                     (t
                      (vector byte))))))))

(defclass kermit-tx-quantum ()
  ((data           :initarg :data           :reader quantum-data)
   (num-user-bytes :initarg :num-user-bytes :reader quantum-num-user-bytes))
  (:documentation "An indivisible sequence of data bytes to be embedded in a packet."))

(defmethod encode-data-item ((kpc kermit-protocol-controller) (data fixnum))
  "Return a KERMIT-TX-QUANTUM holding an indivisible sequence of byte values."
  (make-instance 'kermit-tx-quantum
                 :data (encode-byte kpc data)
                 :num-user-bytes 1))

(defmethod encode-data-item ((kpc kermit-protocol-controller) (data character))
  "Return a KERMIT-TX-QUANTUM holding an indivisible sequence of byte values."
  (make-instance 'kermit-tx-quantum
                 :data (encode-byte kpc (unicode-char-to-hp-code data))
                 :num-user-bytes 1))

(defmethod encode-repeated-item ((kpc kermit-protocol-controller) item rpt-count)
  "Encode a repeated item using repeat count prefixing.
ITEM may be a character or a byte value.  Return a list of
KERMIT-TX-QUANTUMs holding indivisible sequences of byte
values (vectors of byte values that should not be split across packet
boundaries)."
  (assert (>= rpt-count 1))
  (with-accessors ((repeat-prefix-byte repeat-prefix-byte))
      (current-parameters kpc)
    (loop
       :for n := rpt-count :then (- n 94)
       :while (> n 0)
       :collect (if (>= rpt-count 2)
                    (make-instance 'kermit-tx-quantum
                                   :data (concatenate 'vector
                                                      (vector repeat-prefix-byte (encode-tochar (min n 94)))
                                                      (quantum-data (encode-data-item kpc item)))
                                   :num-user-bytes (min n 94))
                    (encode-data-item kpc item)))))

(defmethod encode-data-to-quanta ((kpc kermit-protocol-controller) (data vector))
  "Encode a vector of data (characters or byte values).
Return a list of KERMIT-TX-QUANTUMs (holding indivisible sequences of
byte values that should not be split across packet boundaries)."
  (with-accessors ((repeat-prefix-enable repeat-prefix-enable))
      (current-parameters kpc)
    (if repeat-prefix-enable
      (let ((current-item (aref data 0))
            (current-count 0)
            (result-list nil))
        (dotimes (n (length data))
          (if (eql (aref data n) current-item)
              (incf current-count)
              (progn (setf result-list (append (encode-repeated-item kpc current-item current-count) result-list))
                     (setf current-item (aref data n))
                     (setf current-count 1))))
        (when (> current-count 0)
          (setf result-list (append (encode-repeated-item kpc current-item current-count) result-list)))
        (reverse result-list))
      (loop :for b :across data
            :collect (encode-data-item kpc b)))))

(defclass kermit-tx-chunk ()
  ((data           :initarg :data           :reader chunk-data)
   (num-user-bytes :initarg :num-user-bytes :reader chunk-num-user-bytes))
  (:documentation "A chunk of data suitable for creating a single packet."))

(defun make-chunk-from-quanta (quanta)
  "Create a KERMIT-TX-CHUNK from a list of KERMIT-TX-QUANTUMs."
  (let ((data-list nil)
        (num-user-bytes 0))
    (dolist (quantum quanta)
      (push (quantum-data quantum) data-list)
      (incf num-user-bytes (quantum-num-user-bytes quantum)))
    (make-instance 'kermit-tx-chunk
                   :data (apply #'concatenate 'vector (reverse data-list))
                   :num-user-bytes num-user-bytes)))

(defmethod encode-data-to-chunks ((kpc kermit-protocol-controller) (data vector))
  "Encode a vector of data (characters or byte values).
Return a list of KERMIT-TX-CHUNKs, each one suitable to be the data
portion of a packet."
  (with-accessors ((checksum-size        checksum-size)
                   (tx-max-packet-length tx-max-packet-length))
      (current-parameters kpc)
    (let ((quanta (encode-data-to-quanta kpc data))
          (max-data-length (- tx-max-packet-length checksum-size 2))
          (current-length 0)
          (current-quanta nil)
          (chunk-list))
      (dolist (quantum quanta)
        (let ((len (length (quantum-data quantum))))
          (if (<= (+ current-length len) max-data-length)
              (progn (push quantum current-quanta)
                     (incf current-length len))
              (progn (push (make-chunk-from-quanta (reverse current-quanta)) chunk-list)
                     (setf current-quanta (list quantum))
                     (setf current-length len)))))
      (when (> current-length 0)
        (push (make-chunk-from-quanta (reverse current-quanta)) chunk-list))
      (reverse chunk-list))))

(defmethod encode-data ((kpc kermit-protocol-controller) (data vector))
  "Encode a vector of data (characters or byte values).
Return a vector of byte values."
  (let ((quanta (encode-data-to-quanta kpc data)))
    (apply #'concatenate 'vector (mapcar #'quantum-data quanta))))

(defmethod decode-fragment ((kpc kermit-protocol-controller) (data vector) index &key (allow-repeat-prefix t))
  "Decode a fragment of data.
Accept a vector of byte values and an index into that vector. Return two
values: a decoded fragment of data (as a vector of byte values), and the index
to use to decode the next fragment."
  (let ((ctrl-quote-byte (rx-ctrl-quote-byte (current-parameters kpc)))
        (binary-quote-byte (and (binary-quote-enable (current-parameters kpc))
                                (binary-quote-byte (current-parameters kpc))))
        (repeat-prefix-byte (and (repeat-prefix-enable (current-parameters kpc))
                                 (repeat-prefix-byte (current-parameters kpc))))
        (indexed-byte (aref data index)))
    (cond ((eql indexed-byte ctrl-quote-byte)
           (let ((next-byte (aref data (1+ index))))
             (cond ((eql (mod next-byte 128) ctrl-quote-byte)
                    (values (vector next-byte) (+ index 2)))
                   ((and binary-quote-byte (eql (mod next-byte 128) binary-quote-byte))
                    (values (vector next-byte) (+ index 2)))
                   ((and repeat-prefix-byte (eql (mod next-byte 128) repeat-prefix-byte))
                    (values (vector next-byte) (+ index 2)))
                   (t
                    (values (vector (code-ctl next-byte)) (+ index 2))))))
          ((eql indexed-byte binary-quote-byte)
           (multiple-value-bind (byt idx) (decode-fragment kpc data (1+ index) :allow-repeat-prefix nil)
             (values (map 'vector (lambda (x) (logior x 128)) byt) idx)))
          ((and allow-repeat-prefix (eql indexed-byte repeat-prefix-byte))
           (let ((repeat-count (decode-unchar (aref data (1+ index)))))
             (multiple-value-bind (byt idx) (decode-fragment kpc data (+ index 2) :allow-repeat-prefix nil)
               (values (make-array repeat-count :initial-element (aref byt 0)) idx))))
          (t
           (values (vector indexed-byte) (1+ index))))))

(defmethod decode-data ((kpc kermit-protocol-controller) (data vector))
  "Decode data from a received packet.
Accept a vector of byte values containing raw data from a received packet,
and return a vector of decoded byte values."
  (let ((fragments nil)
        (index 0))
    (loop :while (< index (length data))
          :do (multiple-value-bind (fragment next-index) (decode-fragment kpc data index)
                (push fragment fragments)
                (setf index next-index)))
    (apply #'concatenate 'vector (reverse fragments))))

;;;
;;; Routines to process flow from calculator to computer
;;;
(defmethod process-param-request  ((kpc kermit-protocol-controller) data)
  "Process parameter data received in an S or I packet."
  (let ((pending (pending-parameters kpc)))
    (set-default-params pending)
    (when (>= (length data) 1)
      (setf (tx-max-packet-length pending) (decode-unchar (elt data 0))))      ; MAXL
    (when (and (>= (length data) 2) (honour-timeout-period-request kpc))
      (setf (hoppi-timeout-period pending) (decode-unchar (elt data 1))))      ; TIME
    (when (>= (length data) 3)
      (setf (tx-num-pad-bytes pending) (decode-unchar (elt data 2))))          ; NPAD
    (when (>= (length data) 4)
      (setf (tx-pad-byte pending) (code-ctl (elt data 3))))                    ; PADC
    (when (>= (length data) 5)
      (setf (tx-packet-terminator-byte pending) (decode-unchar (elt data 4)))) ; EOL
    (when (>= (length data) 6)
      (setf (rx-ctrl-quote-byte pending) (elt data 5)))                        ; QCTL
    (when (>= (length data) 7)
      (setf (binary-quote-byte pending) (elt data 6)))                         ; QBIN
    (when (>= (length data) 8)
      (setf (checksum-size pending) (- (elt data 7) (char-code #\0))))         ; CHKT
    (when (>= (length data) 9)
      (setf (repeat-prefix-byte pending) (elt data 8)))                        ; REPT
    (let ((binary-quote-byte (binary-quote-byte pending)))
      (if (or (= binary-quote-byte (char-code #\Y))
              (= binary-quote-byte (char-code #\N))
              (< binary-quote-byte 33)
              (and (> binary-quote-byte 62) (< binary-quote-byte 96))
              (> binary-quote-byte 126))
        (setf (binary-quote-enable pending) nil)
        (setf (binary-quote-enable pending) t)))
    (if (eql (repeat-prefix-byte pending) (char-code #\Space))
        (setf (repeat-prefix-enable pending) nil)
        (setf (repeat-prefix-enable pending) t))))

(defmethod process-i-packet ((kpc kermit-protocol-controller) (pkt kermit-packet))
  "Process an 'I' packet -- initialization."
  (process-param-request kpc (packet-data pkt (checksum-size (current-parameters kpc)))))

(defmethod process-s-packet ((kpc kermit-protocol-controller) (pkt kermit-packet))
  "Process an 'S' packet -- send initialization."
  (process-param-request kpc (packet-data pkt (checksum-size (current-parameters kpc)))))

(defmethod construct-param-response ((kpc kermit-protocol-controller))
  "Construct parameter data to send in an ACK response to an S or I packet."
  (let ((pending (pending-parameters kpc)))
    (vector (encode-tochar +max-packet-size+)                   ; MAXL
            (encode-tochar (calculator-timeout-period pending)) ; TIME
            (encode-tochar 0)                                   ; NPAD
            (code-ctl 0)                                        ; PADC
            (encode-tochar 13)                                  ; EOL
            (tx-ctrl-quote-byte pending)                        ; QCTL
            (binary-quote-byte pending)                         ; QBIN
            (+ (checksum-size pending) (char-code #\0))         ; CHKT
            (repeat-prefix-byte pending))))                     ; REPT

(defmethod send-ack-with-params ((kpc kermit-protocol-controller) &optional seq-num)
  "Send an ACK packet with initialisation parameters."
  (let ((seq-num (or seq-num (seq-num kpc)))
        (checksum-size (checksum-size (current-parameters kpc)))
        (data (construct-param-response kpc)))
    (push-packet (tx-buffer kpc) (make-kermit-packet seq-num #\Y :data data :checksum-size checksum-size))))

(defmethod process-c-packet ((kpc kermit-protocol-controller) (pkt kermit-packet))
  "Process a 'C' packet -- host command.
On failure of the command return NIL and an error message string, otherwise
return a title string and a response string to send back to the calculator,
or T and NIL if there is nothing to send back."
  (let ((command (map 'string #'hp-code-to-unicode-char
                      (decode-data kpc (packet-data pkt (checksum-size (current-parameters kpc))))))
        (workspace (workspace kpc)))
    (declare (ignore command))
    ;; !!! TODO -- check command and dispatch appropriately !!!
    ;; !!! for now just assume it's 'DIR /W' from an HP48 !!!
    (close-current-input-stream workspace)
    (multiple-value-bind (listing dirpath)
        (list-folder-for-reading workspace (current-folder-path workspace) :style :DOS)
      (if listing
        (values "File listing" listing)
        (values nil (format nil "Cannot list directory: ~A" dirpath))))))

(defun extract-generic-command-parameters (data)
  "Decode and extract a generic command's parameters.
Returns a list of strings."
  (loop :for base := 0 :then (+ base len 1)
        :while (< base (length data))
        :for len := (decode-unchar (aref data base))
        :while (<= (+ base len 1) (length data))
        :collect (map 'string #'hp-code-to-unicode-char (subseq data (1+ base) (+ base len 1)))))

(defmethod process-g-packet ((kpc kermit-protocol-controller) (pkt kermit-packet))
  "Process a 'G' packet -- generic Kermit command.
On failure of the command return NIL and an error message string, otherwise
return a title string and a response string to send back to the calculator,
or T and NIL if there is nothing to send back."
  (let* ((workspace (workspace kpc))
         (checksum-size (checksum-size (current-parameters kpc)))
         (data (decode-data kpc (packet-data pkt checksum-size)))
         (cmd (and (>= (length data) 1)
                   (code-char (aref data 0)))))
    (case cmd
      ((#\C)  ;; Change directory
       (let* ((params (extract-generic-command-parameters (subseq data 1)))
              (dirname (first params)))
         (multiple-value-bind (success msg)
             (update-current-folder-path workspace (or dirname (namestring-home-folder)))
           (if success
             (values "Current directory" msg)
             (values nil msg)))))
      ((#\D)  ;; List directory contents
       (let* ((params (extract-generic-command-parameters (subseq data 1)))
              (dirname (first params)))
         (close-current-input-stream workspace)
         (multiple-value-bind (listing dirpath)
             (list-folder-for-reading workspace dirname :style :RPL)
           (if listing
             (values "File listing" listing)
             (values nil (format nil "Cannot list directory: ~A" dirpath))))))
      ((#\F)  ;; Finish command (ignore it)
       (values t nil))
      ((#\V)  ;; Variable set/get -- currently very limited
       (let* ((params (extract-generic-command-parameters (subseq data 1)))
              (cmd (first params))
              (name (second params))
              (value (third params)))
         (cond ((and (equalp cmd "S")  ; set allow-overwrite on
                     (equalp name "ALLOW-OVERWRITE")
                     (equalp value "ON"))
                (setf (overwrite-action workspace) :supersede)
                (values "Var ALLOW-OVERWRITE" "ON"))
               ((and (equalp cmd "S")  ; set allow-overwrite off
                     (equalp name "ALLOW-OVERWRITE")
                     (equalp value "OFF"))
                (setf (overwrite-action workspace) nil)
                (values "Var ALLOW-OVERWRITE" "OFF"))
               ((and (equalp cmd "Q")  ; query allow-overwrite status
                     (equalp name "ALLOW-OVERWRITE"))
                (values "Var ALLOW-OVERWRITE" (if (overwrite-action workspace) "ON" "OFF")))
               ((and (equalp cmd "S")  ; set next transfer-type
                     (equalp name "TRANSFER-TYPE"))
                (if (equalp value "TEXT-AS-STRING")
                    (setf (transfer-type kpc) :text-as-string)
                    (setf (transfer-type kpc) nil))
                (values "Var TRANSFER-TYPE" (string (transfer-type kpc))))
               ((and (equalp cmd "Q")  ; query next transfer-type status
                     (equalp name "TRANSFER-TYPE"))
                (values "Var TRANSFER-TYPE" (string (transfer-type kpc))))
               (t
                (values nil "Unknown var/cmd")))))
      (t
       (values nil "Unsupported command")))))

(defmethod process-r-packet ((kpc kermit-protocol-controller) (pkt kermit-packet))
  "Process an 'R' packet -- receive initialization.
Return T if the requested file exists and can be opened for reading,
otherwise return NIL.  Also return a string as secondary result: on
success return the name of the file opened, and on failure return an
error message."
  (let ((filename (map 'string #'hp-code-to-unicode-char
                       (decode-data kpc (packet-data pkt (checksum-size (current-parameters kpc))))))
        (workspace (workspace kpc)))
    (close-current-input-stream workspace)
    (let ((transfer-type (transfer-type kpc)))
      (setf (transfer-type kpc) nil)
      (open-file-for-reading workspace filename transfer-type))))

(defmethod process-f-packet ((kpc kermit-protocol-controller) (pkt kermit-packet))
  "Process an 'F' packet -- file header.
Return T if the requested file can be opened for writing, otherwise NIL.
Also return a string as secondary result: on success return the name of
the file opened (which may be different to that requested, e.g. with a
suffix to prevent overwriting a file), and on failure return an error
message."
  (let ((filename (map 'string #'hp-code-to-unicode-char
                       (decode-data kpc (packet-data pkt (checksum-size (current-parameters kpc))))))
        (workspace (workspace kpc)))
    (close-current-output-stream workspace)
    (let ((transfer-type (transfer-type kpc)))
      (setf (transfer-type kpc) nil)
      (if (zerop (length filename))
          (open-message-for-writing workspace "(unnamed file transfer)" transfer-type)
          (open-file-for-writing workspace filename transfer-type)))))

(defmethod process-x-packet ((kpc kermit-protocol-controller) (pkt kermit-packet))
  "Process an 'X' packet -- message header.
Return T if the requested message title can be handled, otherwise NIL.
Also return a string as secondary result: on success return the message
title and on failure return an error message."
  (let ((title (map 'string #'hp-code-to-unicode-char
                    (decode-data kpc (packet-data pkt (checksum-size (current-parameters kpc))))))
        (workspace (workspace kpc)))
    (close-current-output-stream workspace)
    (let ((transfer-type (transfer-type kpc)))
      (setf (transfer-type kpc) nil)
      (open-message-for-writing workspace title transfer-type))))

(defmethod process-d-packet ((kpc kermit-protocol-controller) (pkt kermit-packet))
  "Process a 'D' packet -- data packet.
Return T if data was written successfully, otherwise return NIL.
Also return a string as secondary result: on success return the path of
the file to which we are writing, and on failure return an error message."
  (let ((data (decode-data kpc (packet-data pkt (checksum-size (current-parameters kpc)))))
        (workspace (workspace kpc)))
    (write-to-current-output-stream workspace data)))

(defmethod process-z-packet ((kpc kermit-protocol-controller) (pkt kermit-packet))
  "Process a 'Z' packet -- end of file."
  ;; !!! TODO should really check for a D in the data field -- meaning discard file
  (let ((workspace (workspace kpc)))
    (close-current-output-stream workspace)))

(defmethod process-e-packet ((kpc kermit-protocol-controller) (pkt kermit-packet))
  "Process an 'E' packet -- error indication."
  (let ((msg (map 'string #'hp-code-to-unicode-char
                  (decode-data kpc (packet-data pkt (checksum-size (current-parameters kpc)))))))
    (setf (rx-error-message kpc) msg)))

(defmethod send-ack ((kpc kermit-protocol-controller) &optional seq-num)
  "Send an empty ACK packet."
  (let ((seq-num (or seq-num (seq-num kpc)))
        (checksum-size (checksum-size (current-parameters kpc))))
    (push-packet (tx-buffer kpc) (make-kermit-packet seq-num #\Y :data #() :checksum-size checksum-size))))

(defmethod send-nak ((kpc kermit-protocol-controller) &optional seq-num)
  "Send a NAK packet."
  (let ((seq-num (or seq-num (seq-num kpc)))
        (checksum-size (checksum-size (current-parameters kpc))))
    (push-packet (tx-buffer kpc) (make-kermit-packet seq-num #\N :data #() :checksum-size checksum-size))))

(defmethod send-e-packet ((kpc kermit-protocol-controller) message)
  "Send an error packet with the given message."
  (setf (tx-error-message kpc) message)
  (push-packet (tx-buffer kpc)
               (make-kermit-packet (seq-num kpc)
                                   #\E
                                   :data (chunk-data (first (encode-data-to-chunks kpc message))) ; truncate message if necessary
                                   :checksum-size (checksum-size (current-parameters kpc)))))

;;;
;;; Routines to process flow from computer to calculator
;;;
(defmethod construct-param-request ((kpc kermit-protocol-controller))
  "Construct parameter data to send in an S or I packet."
  (let ((pending (pending-parameters kpc)))
    (set-default-params pending)
    (setf (tx-max-packet-length pending) +max-packet-size+)
    (setf (checksum-size pending) 3)
    (vector (encode-tochar (tx-max-packet-length pending))      ; MAXL
            (encode-tochar (calculator-timeout-period pending)) ; TIME
            (encode-tochar (tx-num-pad-bytes pending))          ; NPAD
            (code-ctl (tx-pad-byte pending))                    ; PADC
            (encode-tochar (tx-packet-terminator-byte pending)) ; EOL
            (tx-ctrl-quote-byte pending)                        ; QCTL
            (binary-quote-byte pending)                         ; QBIN
            (+ (checksum-size pending) (char-code #\0))         ; CHKT
            (repeat-prefix-byte pending))))                     ; REPT

(defmethod send-s-packet ((kpc kermit-protocol-controller) &optional seq-num checksum-size)
  "Send a send-initiation packet."
  (let ((seq-num (or seq-num (seq-num kpc)))
        (checksum-size (or checksum-size (checksum-size (current-parameters kpc))))
        (data (construct-param-request kpc)))
    (push-packet (tx-buffer kpc) (make-kermit-packet seq-num #\S :data data :checksum-size checksum-size))))

(defmethod send-i-packet ((kpc kermit-protocol-controller) &optional seq-num checksum-size)
  "Send an initialize packet."
  (let ((seq-num (or seq-num (seq-num kpc)))
        (checksum-size (or checksum-size (checksum-size (current-parameters kpc))))
        (data (construct-param-request kpc)))
    (push-packet (tx-buffer kpc) (make-kermit-packet seq-num #\I :data data :checksum-size checksum-size))))

(defmethod process-param-response ((kpc kermit-protocol-controller) data)
  "Process parameter data received in an ACK response to an S or I packet."
  (let ((pending (pending-parameters kpc)))
    (when (>= (length data) 1)
      (setf (tx-max-packet-length pending) (decode-unchar (elt data 0))))       ; MAXL
    (when (and (>= (length data) 2) (honour-timeout-period-request kpc))
      (setf (hoppi-timeout-period pending) (decode-unchar (elt data 1))))       ; TIME
    (when (>= (length data) 3)
      (setf (tx-num-pad-bytes pending) (decode-unchar (elt data 2))))           ; NPAD
    (when (>= (length data) 4)
      (setf (tx-pad-byte pending) (code-ctl (elt data 3))))                     ; PADC
    (when (>= (length data) 5)
      (setf (tx-packet-terminator-byte pending) (decode-unchar (elt data 4))))  ; EOL
    (when (>= (length data) 6)
      (setf (rx-ctrl-quote-byte pending) (elt data 5)))                         ; QCTL
    (when (>= (length data) 7)
      (setf (binary-quote-byte pending) (elt data 6)))                          ; QBIN
    (when (>= (length data) 8)
      (setf (checksum-size pending) (- (elt data 7) (char-code #\0))))          ; CHKT
    (when (>= (length data) 9)
      (setf (repeat-prefix-byte pending) (elt data 8)))                         ; REPT
    (let ((binary-quote-byte (binary-quote-byte pending)))
      (if (or (= binary-quote-byte (char-code #\Y))
              (= binary-quote-byte (char-code #\N))
              (< binary-quote-byte 33)
              (and (> binary-quote-byte 62) (< binary-quote-byte 96))
              (> binary-quote-byte 126))
        (setf (binary-quote-enable pending) nil)
        (setf (binary-quote-enable pending) t)))
    (if (eql (repeat-prefix-byte pending) (char-code #\Space))
        (setf (repeat-prefix-enable pending) nil)
        (setf (repeat-prefix-enable pending) t)))
  (activate-pending-parameters kpc))

(defmethod process-ack-with-params ((kpc kermit-protocol-controller) (pkt kermit-packet))
  "Process a 'Y' packet received in acknowledgement to an 'S' or 'I' packet."
  (process-param-response kpc (packet-data pkt (checksum-size (current-parameters kpc)))))

(defmethod send-f-packet ((kpc kermit-protocol-controller))
  "Send a file header packet."
  (let* ((path (current-input-name (workspace kpc)))
         (name (or (and path (namestring-file-part path)) ""))
         (data (chunk-data (first (encode-data-to-chunks kpc name))))
         (checksum-size (checksum-size (current-parameters kpc))))
    (push-packet (tx-buffer kpc) (make-kermit-packet (seq-num kpc) #\F :data data :checksum-size checksum-size))))

(defmethod send-x-packet ((kpc kermit-protocol-controller))
  "Send a text header packet."
  (let* ((title (current-input-name (workspace kpc)))
         (name (or title ""))
         (data (chunk-data (first (encode-data-to-chunks kpc name))))
         (checksum-size (checksum-size (current-parameters kpc))))
    (push-packet (tx-buffer kpc) (make-kermit-packet (seq-num kpc) #\X :data data :checksum-size checksum-size))))

(defmethod send-d-packet ((kpc kermit-protocol-controller))
  "Send data packet."
  (let ((chunk (first (pending-tx-chunks kpc)))
        (checksum-size (checksum-size (current-parameters kpc))))
    (incf (num-user-bytes-sent kpc) (chunk-num-user-bytes chunk))
    (setf (pending-tx-chunks kpc) (rest (pending-tx-chunks kpc)))
    (push-packet (tx-buffer kpc) (make-kermit-packet (seq-num kpc) #\D
                                                     :data (chunk-data chunk)
                                                     :checksum-size checksum-size))))

(defmethod send-z-packet ((kpc kermit-protocol-controller))
  "Send end-of-file packet."
  (let ((checksum-size (checksum-size (current-parameters kpc))))
    (push-packet (tx-buffer kpc) (make-kermit-packet (seq-num kpc) #\Z :data #() :checksum-size checksum-size))))

(defmethod send-b-packet ((kpc kermit-protocol-controller))
  "Send break packet."
  (let ((checksum-size (checksum-size (current-parameters kpc))))
    (push-packet (tx-buffer kpc) (make-kermit-packet (seq-num kpc) #\B :data #() :checksum-size checksum-size))))

;;;
;;; The Kermit protocol state-machine
;;;
(defun match-packet-p (pkt &key seq type subtype)
  "Match packet against type, subtype and/or sequence number.
Return T if pkt is a Kermit packet and, if any specified, matches the
given sequence number, type and subtype values. Otherwise return NIL."
  (let ((type (if (characterp type) (char-code type) type))
        (subtype (if (characterp subtype) (char-code subtype) subtype)))
    (and (typep pkt 'kermit-packet)
         (or (not seq)     (eql (packet-seq pkt) seq))
         (or (not type)    (eql (packet-type pkt) type))
         (or (not subtype) (eql (aref (packet-data+chk pkt) 0) subtype)))))

(defun incr-seq (kpc)
  "Increment the sequence number modulo 64 and set the retry count to zero."
  (setf (seq-num kpc) (mod (1+ (seq-num kpc)) 64))
  (setf (retry-count kpc) 0))

(defmethod run-protocol-step ((kpc kermit-protocol-controller) &key pkt tick)
  "Run the Kermit protocol controller's state machine.
Optionally pass in a received packet and/or timer tick event. Return T if
this function should be immediately called again to permit further processing,
or else return NIL to indicate that it need only be called when another
packet/timer-event is available.  Also return a second value, a keyword
indicating the current state."
  ;; Some macros to facilitate easy control of the state-machine
  (dbg-msg "RUN-PROTOCOL-STEP: ~A ~A ~A" kpc pkt tick)
  (macrolet ((run-yield ()
               '(return-from run-protocol-step (values nil protocol-state)))
             (run-continue (next-state)
               (check-type next-state keyword)
               `(progn (setf protocol-state ,next-state)
                       (return-from run-protocol-step (values t ,next-state))))
             (bump-retry (error-message error-state)
               (check-type error-message string)
               (check-type error-state keyword)
               `(progn (incf retry-count)
                       (when (>= retry-count retry-limit)
                         (send-e-packet kpc ,error-message)
                         (setf protocol-state ,error-state)
                         (return-from run-protocol-step (values t ,error-state))))))
    ;; Main state-machine
    (with-accessors ((protocol-state protocol-state)
                     (seq-num seq-num)
                     (retry-count retry-count)
                     (retry-limit retry-limit)
                     (timer-count timer-count)
                     (send-idle-nak send-idle-nak)
                     (pending-tx-chunks pending-tx-chunks)
                     (num-user-bytes-sent num-user-bytes-sent)
                     (workspace workspace))
        kpc
      (let ((hoppi-timeout-period (hoppi-timeout-period (current-parameters kpc)))
            (checksum-size (checksum-size (current-parameters kpc))))
        (when tick
          (incf timer-count))
        (when pkt
          (setf timer-count 0))
        (case protocol-state
          ;;
          ;; RECEIVE-SERVER-IDLE -- server idle, waiting for a message
          ;;
          ((:receive-server-idle)
           (dbg-msg ":receive-server-idle")
           (setf seq-num 0)
           (setf retry-count 0)
           (cond ((match-packet-p pkt :type #\I :seq 0)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (process-i-packet kpc pkt)
                  (send-ack-with-params kpc 0)
                  (run-yield))
                 ((match-packet-p pkt :type #\S :seq 0)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (process-s-packet kpc pkt)
                  (send-ack-with-params kpc 0)
                  (incr-seq kpc)
                  (run-continue :receive-file))
                 ((match-packet-p pkt :type #\R :seq 0)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (multiple-value-bind (success msg) (process-r-packet kpc pkt)
                    (when success
                      (run-continue :send-init))
                    (send-e-packet kpc msg)
                    (run-continue :abort)))
                 ((match-packet-p pkt :type #\K :seq 0)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  ;; !!! TODO not yet implemented !!!
                  (send-e-packet kpc "K not implemented")
                  (run-yield))
                 ((match-packet-p pkt :type #\C :seq 0)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (multiple-value-bind (success response) (process-c-packet kpc pkt)
                    (when (and success response)
                      (open-message-for-reading workspace success response :text)
                      (run-continue :send-init))
                    (when (and success (not response))
                      (send-ack kpc)
                      (incr-seq kpc)
                      (run-continue :complete))
                    (send-e-packet kpc response)
                    (run-continue :abort)))
                 ((match-packet-p pkt :type #\G :seq 0)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (multiple-value-bind (success response) (process-g-packet kpc pkt)
                    (when (and success response)
                      (open-message-for-reading workspace success response :text)
                      (run-continue :send-init))
                    (when (and success (not response))
                      (send-ack kpc)
                      (incr-seq kpc)
                      (run-continue :complete))
                    (send-e-packet kpc response)
                    (run-continue :abort)))
                 ((match-packet-p pkt :type #\E)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (process-e-packet kpc pkt)
                  (run-continue :abort))
                 ((>= timer-count hoppi-timeout-period)
                  (setf timer-count 0)
                  (when send-idle-nak
                    (send-nak kpc 0))
                  (run-yield))
                 ((match-packet-p pkt)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (send-e-packet kpc "Packet not understood")
                  (run-yield))
                 (t
                  (run-yield))))
          ;;
          ;; RECEIVE-INIT -- entry point for non-server RECEIVE command
          ;;
          ((:receive-init)
           (dbg-msg ":receive-init")
           (setf seq-num 0)
           (setf retry-count 0)
           (cond ((match-packet-p pkt :type #\S :seq 0)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (process-s-packet kpc pkt)
                  (send-ack-with-params kpc 0)
                  (incr-seq kpc)
                  (run-continue :receive-file))
                 ((match-packet-p pkt :type #\E)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (process-e-packet kpc pkt)
                  (run-continue :abort))
                 ((>= timer-count hoppi-timeout-period)
                  (setf timer-count 0)
                  (send-nak kpc 0)
                  (bump-retry "Timed out" :abort)
                  (run-yield))
                 ((match-packet-p pkt)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (send-e-packet kpc "Packet not understood")
                  (run-continue :abort))
                 (t
                  (run-yield))))
          ;;
          ;; RECEIVE-FILE -- look for a file header or EOT packet
          ;;
          ((:receive-file)
           (dbg-msg ":receive-file")
           (cond ((match-packet-p pkt :type #\F :seq seq-num)
                  (unless (verify-checksum-p pkt (checksum-size (pending-parameters kpc)))
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (activate-pending-parameters kpc)
                  (multiple-value-bind (success msg) (process-f-packet kpc pkt)
                    (when success
                      (send-ack kpc)
                      (incr-seq kpc)
                      (run-continue :receive-data))
                    (send-e-packet kpc msg)
                    (run-continue :abort)))
                 ((match-packet-p pkt :type #\X :seq seq-num)
                  (unless (verify-checksum-p pkt (checksum-size (pending-parameters kpc)))
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (activate-pending-parameters kpc)
                  (multiple-value-bind (success msg) (process-x-packet kpc pkt)
                    (when success
                      (send-ack kpc)
                      (incr-seq kpc)
                      (run-continue :receive-data))
                    (send-e-packet kpc msg)
                    (run-continue :abort)))
                 ((match-packet-p pkt :type #\B :seq seq-num)
                  (unless (verify-checksum-p pkt (checksum-size (pending-parameters kpc)))
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (activate-pending-parameters kpc)
                  (send-ack kpc)
                  (run-continue :complete))
                 ((match-packet-p pkt :type #\S :seq (1- seq-num))
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc (1- seq-num))
                    (run-yield))
                  (send-ack-with-params kpc (1- seq-num))
                  (bump-retry "Timed out" :abort)
                  (run-yield))
                 ((match-packet-p pkt :type #\Z :seq (1- seq-num))
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc (1- seq-num))
                    (run-yield))
                  (send-ack kpc (1- seq-num))
                  (bump-retry "Timed out" :abort)
                  (run-yield))
                 ((match-packet-p pkt :type #\E)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (process-e-packet kpc pkt)
                  (run-continue :abort))
                 ((>= timer-count hoppi-timeout-period)
                  (setf timer-count 0)
                  (send-ack kpc)
                  (bump-retry "Timed out" :abort)
                  (run-yield))
                 ((match-packet-p pkt)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (send-e-packet kpc "Packet not understood")
                  (run-continue :abort))
                 (t
                  (run-yield))))
          ;;
          ;; RECEIVE-DATA -- receive data up to end of file
          ;;
          ((:receive-data)
           (dbg-msg ":receive-data")
           (cond ((match-packet-p pkt :type #\D :seq seq-num)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (multiple-value-bind (success msg) (process-d-packet kpc pkt)
                    (when success
                      (send-ack kpc)
                      (incr-seq kpc)
                      (run-yield))
                    (send-e-packet kpc msg)
                    (run-continue :abort)))
                 ((match-packet-p pkt :type #\D :seq (1- seq-num))
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc (1- seq-num))
                    (run-yield))
                  (send-ack kpc (1- seq-num))
                  (bump-retry "Timed out" :abort)
                  (run-yield))
                 ((match-packet-p pkt :type #\Z :seq seq-num)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (process-z-packet kpc pkt)
                  (send-ack kpc)
                  (incr-seq kpc)
                  (run-continue :receive-file))
                 ((match-packet-p pkt :type #\F :seq (1- seq-num))
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc (1- seq-num))
                    (run-yield))
                  (send-ack kpc (1- seq-num))
                  (bump-retry "Timed out" :abort)
                  (run-yield))
                 ((match-packet-p pkt :type #\X :seq (1- seq-num))
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc (1- seq-num))
                    (run-yield))
                  (send-ack kpc (1- seq-num))
                  (bump-retry "Timed out" :abort)
                  (run-yield))
                 ((match-packet-p pkt :type #\E)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (process-e-packet kpc pkt)
                  (run-continue :abort))
                 ((>= timer-count hoppi-timeout-period)
                  (setf timer-count 0)
                  (send-ack kpc (1- seq-num))
                  (bump-retry "Timed out" :abort)
                  (run-yield))
                 ((match-packet-p pkt)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (send-e-packet kpc "Packet not understood")
                  (run-continue :abort))
                 (t
                  (run-yield))))
          ;;
          ;; SEND-INIT -- entry point for the SEND command
          ;;
          ((:send-init)
           (dbg-msg ":send-init")
           (setf seq-num 0)
           (setf retry-count 0)
           (send-s-packet kpc)
           (run-continue :send-init-ack))
          ((:send-init-ack)
           (dbg-msg ":send-init-ack")
           (cond ((match-packet-p pkt :type #\Y :seq 0)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (process-ack-with-params kpc pkt)
                  (incr-seq kpc)
                  (run-continue :open-file))
                 ((match-packet-p pkt :type #\N)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (bump-retry "Timed out" :abort)
                  (run-continue :send-init))
                 ((match-packet-p pkt :type #\E)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (process-e-packet kpc pkt)
                  (run-continue :abort))
                 ((>= timer-count hoppi-timeout-period)
                  (setf timer-count 0)
                  (bump-retry "Timed out" :abort)
                  (run-continue :send-init))
                 ((match-packet-p pkt)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (bump-retry "Timed out" :abort)
                  (run-continue :send-init))
                 (t
                  (run-yield))))
          ;;
          ;; OPEN-FILE -- open file or setup text to send
          ;;
          ((:open-file)
           (dbg-msg ":open-file")
           ;; File should already be opened in workspace when we get
           ;; here (from processing received 'R' packet when remote end
           ;; is pulling a named file, or because we manually opened it
           ;; when we are pushing a file), so let's generate some data
           ;; to send, then go send it.
           (setf pending-tx-chunks (encode-data-to-chunks kpc (read-from-current-input-stream workspace 10240)))
           (setf num-user-bytes-sent 0)
           (run-continue :send-file))
          ;;
          ;; SEND-FILE -- send file or text header
          ;;
          ((:send-file)
           (dbg-msg ":send-file")
           (case (current-input-type workspace)
             ((:file)
              (send-f-packet kpc)
              (run-continue :send-file-ack))
             ((:message)
              (send-x-packet kpc)
              (run-continue :send-file-ack))
             (t
              (setf (protocol-error-message kpc) "Oops -- internal error")
              (run-continue :abort))))
          ((:send-file-ack)
           (dbg-msg ":send-file-ack")
           (cond ((match-packet-p pkt :type #\Y :seq seq-num)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (incr-seq kpc)
                  (run-continue :send-data))
                 ((match-packet-p pkt :type #\N :seq (1+ seq-num))
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (incr-seq kpc)
                  (run-continue :send-data))
                 ((match-packet-p pkt :type #\N)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (bump-retry "Timed out" :abort)
                  (run-continue :send-file))
                 ((match-packet-p pkt :type #\E)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (process-e-packet kpc pkt)
                  (run-continue :abort))
                 ((>= timer-count hoppi-timeout-period)
                  (bump-retry "Timed out" :abort)
                  (run-continue :send-file))
                 ((match-packet-p pkt)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (send-e-packet kpc "Packet not understood")
                  (run-continue :abort))
                 (t
                  (run-yield))))
          ;;
          ;; SEND-DATA -- send contents of file or textual information
          ;;
          ((:send-data)
           (dbg-msg ":send-data")
           (cond (pending-tx-chunks
                  (send-d-packet kpc)
                  (run-continue :send-data-ack))
                 ((eof-current-input-stream-p workspace)
                  (run-continue :send-eof))
                 (t
                  (let ((data (read-from-current-input-stream workspace 10240)))
                    (when (zerop (length data))
                      (run-continue :send-eof))
                    (setf pending-tx-chunks (encode-data-to-chunks kpc data))
                    (send-d-packet kpc)
                    (run-continue :send-data-ack)))))
          ((:send-data-ack)
           (dbg-msg ":send-data-ack")
           (cond ((match-packet-p pkt :type #\Y :seq seq-num)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (incr-seq kpc)
                  (run-continue :send-data))
                 ((match-packet-p pkt :type #\N :seq (1+ seq-num))
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (incr-seq kpc)
                  (run-continue :send-data))
                 ((match-packet-p pkt :type #\Y :subtype #\X :seq seq-num )
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (incr-seq kpc)
                  (run-continue :send-eof))
                 ((match-packet-p pkt :type #\Y :subtype #\Z :seq seq-num)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (incr-seq kpc)
                  (run-continue :send-eof))
                 ((match-packet-p pkt :type #\N)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (bump-retry "Timed out" :abort)
                  (run-continue :send-data))
                 ((match-packet-p pkt :type #\E)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (process-e-packet kpc pkt)
                  (run-continue :abort))
                 ((>= timer-count hoppi-timeout-period)
                  (bump-retry "Timed out" :abort)
                  (run-continue :send-data))
                 ((match-packet-p pkt)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (send-e-packet kpc "Packet not understood")
                  (run-continue :abort))
                 (t
                  (run-yield))))
          ;;
          ;; SEND-EOF -- send end-of-file indicator
          ;;
          ((:send-eof)
           (dbg-msg ":send-eof")
           (send-z-packet kpc)
           (run-continue :send-eof-ack))
          ((:send-eof-ack)
           (dbg-msg ":send-eof-ack")
           (cond ((match-packet-p pkt :type #\Y :seq seq-num)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (incr-seq kpc)
                  (run-continue :send-break))
                 ((match-packet-p pkt :type #\N :seq (1+ seq-num))
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (incr-seq kpc)
                  (run-continue :send-break))
                 ((match-packet-p pkt :type #\N)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (bump-retry "Timed out" :abort)
                  (run-continue :send-eof))
                 ((match-packet-p pkt :type #\E)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (process-e-packet kpc pkt)
                  (run-continue :abort))
                 ((>= timer-count hoppi-timeout-period)
                  (bump-retry "Timed out" :abort)
                  (run-continue :send-eof))
                 ((match-packet-p pkt)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (send-e-packet kpc "Packet not understood")
                  (run-continue :abort))
                 (t
                  (run-yield))))
          ;;
          ;; SEND-BREAK -- end of transaction
          ;;
          ((:send-break)
           (dbg-msg ":send-break")
           (send-b-packet kpc)
           (run-continue :send-break-ack))
          ((:send-break-ack)
           (dbg-msg ":send-break-ack")
           (cond ((match-packet-p pkt :type #\Y :seq seq-num)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (run-continue :complete))
                 ((match-packet-p pkt :type #\N :seq 0)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (run-continue :complete))
                 ((match-packet-p pkt :type #\N :seq seq-num)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (run-continue :send-break))
                 ((match-packet-p pkt :type #\E)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (process-e-packet kpc pkt)
                  (run-continue :abort))
                 ((>= timer-count hoppi-timeout-period)
                  (run-continue :send-break))
                 ((match-packet-p pkt)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (send-e-packet kpc "Packet not understood")
                  (run-continue :abort))
                 (t
                  (run-yield))))
          ;;
          ;; SEND-SERVER-INIT -- entry for server commands which expect large response
          ;;
          ((:send-server-init)
           (dbg-msg ":send-server-init")
           (send-i-packet kpc 0)
           (run-continue :send-server-init-ack))
          ((:send-server-init-ack)
           (dbg-msg ":send-server-init-ack")
           (cond ((match-packet-p pkt :type #\Y :seq 0)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (process-ack-with-params kpc pkt)
                  (run-continue :send-gen-cmd))
                 ((match-packet-p pkt :type #\N)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (bump-retry "Timed out" :abort)
                  (run-continue :send-server-init))
                 ((>= timer-count hoppi-timeout-period)
                  (bump-retry "Timed out" :abort)
                  (run-continue :send-server-init))
                 ((match-packet-p pkt :type #\E)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (set-default-params (current-parameters kpc))
                  (run-continue :send-gen-cmd))
                 ((match-packet-p pkt)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (send-e-packet kpc "Packet not understood")
                  (run-continue :abort))
                 (t
                  (run-yield))))
          ;;
          ;; SEND-GEN-CMD -- entry for server commands that expect short response
          ;;
          ((:send-gen-cmd)
           (dbg-msg ":send-gen-cmd")
           ;; !!! TODO !!!
           (run-continue :send-gen-cmd-ack))
          ((:send-gen-cmd-ack)
           (dbg-msg ":send-gen-cmd-ack")
           (cond ((match-packet-p pkt :type #\S :seq 0)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (process-s-packet kpc pkt)
                  (send-ack-with-params kpc 0)
                  (incr-seq kpc)
                  (run-continue :receive-file))
                 ((match-packet-p pkt :type #\X :seq 1)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc 1)
                    (run-yield))
                  (incr-seq kpc)
                  (run-continue :receive-data))
                 ((match-packet-p pkt :type #\Y :seq 0)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  ;; !!! TODO !!!
                  (run-continue :complete))
                 ((match-packet-p pkt :type #\N)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (run-yield))
                  (bump-retry "Timed out" :abort)
                  (run-continue :send-gen-cmd))
                 ((match-packet-p pkt :type #\E)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (process-e-packet kpc pkt)
                  (run-continue :abort))
                 ((>= timer-count hoppi-timeout-period)
                  (bump-retry "Timed out" :abort)
                  (run-continue :send-gen-cmd))
                 ((match-packet-p pkt)
                  (unless (verify-checksum-p pkt checksum-size)
                    (setf (protocol-error-message kpc) "Bad checksum")
                    (send-nak kpc)
                    (run-yield))
                  (send-e-packet kpc "Packet not understood")
                  (run-continue :abort))
                 (t
                  (run-yield))))
          ;;
          ;; COMPLETE -- successful completion of transaction
          ;;
          ((:complete)
           (dbg-msg ":complete")
           (setf seq-num 0)
           (setf retry-count 0)
           (set-default-params (current-parameters kpc))
           (run-continue :receive-server-idle))
          ;;
          ;; ABORT -- premature termination of transaction
          ;;
          ((:abort)
           (dbg-msg ":abort")
           (close-current-input-stream workspace)
           (close-current-output-stream workspace)
           (setf seq-num 0)
           (setf retry-count 0)
           (set-default-params (current-parameters kpc))
           (if tick
               (run-continue :restart)
               (run-yield)))
          ;;
          ;; RESTART -- wait for 1 second then clear transmit and
          ;;            receive buffers before restarting
          ;;
          ((:restart)
           (dbg-msg ":restart")
           (reset-buffer (tx-buffer kpc))
           (reset-buffer (rx-buffer kpc))
           (if tick
               (run-continue :restart-now)
               (run-yield)))
          ((:restart-now)
           (dbg-msg ":restart-now")
           (reset-buffer (tx-buffer kpc))
           (reset-buffer (rx-buffer kpc))
           (if tick
               (run-continue :receive-server-idle)
               (run-yield))))))))

(defmethod run-protocol ((kpc kermit-protocol-controller) &key pkt tick)
  "Keep calling run-protocol-step until it requests that we stop.
Supply the the given packet and timer-events on the first call only."
  (dbg-msg "RUN-PROTOCOL: ~A ~A ~A" kpc pkt tick)
  (when (run-protocol-step kpc :pkt pkt :tick tick)
    (loop :while (run-protocol-step kpc))))

(defmethod push-packet ((kpc kermit-protocol-controller) (pkt kermit-packet))
  (dbg-msg "PUSH-PACKET: ~A ~A" kpc pkt)
  (run-protocol kpc :pkt pkt))

(defmethod one-second-tick ((kpc kermit-protocol-controller))
  (run-protocol kpc :tick t))

(defmethod send-file ((kpc kermit-protocol-controller) filename &optional transfer-type)
  (multiple-value-bind (success msg) (open-file-for-reading (workspace kpc) filename transfer-type)
    (cond (success
           (setf (protocol-state kpc) :send-init)
           (run-protocol kpc))
          (t
           (setf (tx-error-message kpc) msg)
           (setf (protocol-state kpc) :abort)
           (run-protocol kpc)))))
