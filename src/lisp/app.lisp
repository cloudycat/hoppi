;;;
;;; Copyright 2012 - 2017 Paul Onions
;;;
;;; Licence: MIT, see LICENCE file for details.
;;;
(in-package :hoppi)

;;; From the CCL library
(require :serial-streams)

;;; Constants
(defconstant +cgfloat+ #+:64-BIT-TARGET 0.0d0 #-:64-BIT-TARGET 0.0)

;;; Application globals
;;;
(defparameter *user-defaults* nil
  "An instance of the USER-DEFAULTS class.")

(defparameter *hoppi-application* nil
  "An instance of the HOPPI-APPLICATION class.")

(defparameter *hoppi-controller* nil
  "An instance of the HOPPI-CONTROLLER class.")

(defparameter *packet-logger* nil
  "An instance of the PACKET-LOGGER class.")

(defparameter *message-logger* nil
  "An instance of the MESSAGE-LOGGER class.")

(defparameter *grob-viewer* nil
  "An instance of the GROB-VIEWER class.")

(defparameter *grob-array* nil
  "A 2D array of GROB data, or NIL if there is none.")

(defparameter *folder-chooser* nil
  "An instance of the NSOpenPanel dialog.")

(defparameter *file-chooser* nil
  "An instance of the NSOpenPanel dialog.")

(defparameter *serial-controller* nil
  "An instance of the SERIAL-CONTROLLER class.")

(defparameter *one-second-timer* nil
  "An instance of NSTimer that activates once per second.")

(defparameter *kpc* nil
  "An instance of the KERMIT-PROTOCOL-CONTROLLER class.")

(defparameter *krxbuf* nil
  "An instance of the KERMIT-RX-BUFFER class.")

(defparameter *ktxbuf* nil
  "An instance of the KERMIT-TX-BUFFER class.")

(defparameter *kwspace* nil
  "An instance of the KERMIT-WORKSPACE class.")

(defparameter *serial-device* nil
  "Namestring path for serial device.")

(defparameter *serial-stream* nil
  "Stream object for serial I/O.")

(defparameter *serial-handle* nil
  "An instance of NSFileHandle for the serial stream.")

(defparameter *nib-path* nil
  "Namestring path to the NIB-file directory during development.
Should be NIL in the standalone executable so that NIB files will be
retrieved from the application bundle.")

(defparameter *lib-path* nil
  "Namestring path to the DYLIB-file directory during development.
Should be NIL in the standalone executable so that DYLIB files will
be retrieved from the application bundle.")

;;; USB callback functions (called from low-level C code)
;;;
(ccl:defcallback usb-post-configure-callback ()
  (report-connection-status "Calculator detected")
  (invoke-usb-async-read))

(ccl:defcallback usb-post-unconfigure-callback ()
  (report-connection-status "Calculator off/disconnected"))

(ccl:defcallback usb-async-read-callback (:address user-data :signed result :unsigned num-bytes-read)
  (declare (ignore result))
  (loop :for n :from 0 :to (1- num-bytes-read)
        :do (push-bytes *krxbuf* (ccl:%get-byte user-data n)))
  (ccl:free user-data)
  (loop :while (packet-available-p *krxbuf*)
        :do (let ((pkt (pop-packet *krxbuf*)))
              (push-packet *kpc* pkt)))
  (loop :while (byte-available-p *ktxbuf*)
        :do (let ((data (pop-bytes *ktxbuf* (min (buffer-level *ktxbuf*) 128))))
              (invoke-usb-async-write data)))
  (invoke-usb-async-read))

(defun invoke-usb-async-read ()
  (let ((buf-ptr (ccl:make-record (:array :char 128))))
    (ccl:external-call "post_usb_async_read" :address buf-ptr :int 128 :address usb-async-read-callback :address buf-ptr :int)))

(ccl:defcallback usb-async-write-callback (:address user-data :signed result :unsigned num-bytes-written)
  (declare (ignore result num-bytes-written))
  (ccl:free user-data))

(defun invoke-usb-async-write (data)
  "Post a USB write request.
Copy data, a vector of byte values, to a C-level output buffer and call
the C function post_usb_async_write() with it. The vector should be less than
128 bytes long, if not it will be truncated."
  (let ((buf-ptr (ccl:make-record (:array :char 128)))
        (data-length (min (length data) 128)))
    (loop :for n :from 0 :to (1- data-length)
          :do (setf (ccl:%get-byte buf-ptr n) (aref data n)))
    (ccl:external-call "post_usb_async_write" :address buf-ptr :int data-length :address usb-async-write-callback :address buf-ptr :int)))

;;; Serial callback functions (called from low-level C code)
;;;
(ccl:defcallback serial-arrivals-callback (:address bsd-path)
  (when (eql (connection-type *user-defaults*) :serial)
    (let ((combo-box (device-path-widget *hoppi-controller*)))
      (when (eql (#/indexOfItemWithObjectValue: combo-box bsd-path) #$NSNotFound)
        (#/addItemWithObjectValue: combo-box bsd-path)
        (let ((dev-str (ccl::lisp-string-from-nsstring bsd-path)))
          (when (equal dev-str (serial-device *user-defaults*))
            (#/selectItemAtIndex: combo-box (#/indexOfItemWithObjectValue: combo-box bsd-path))
            (#/setObjectValue: combo-box (#/objectValueOfSelectedItem combo-box))
            (switch-to-serial-stream dev-str)))))))

(ccl:defcallback serial-departures-callback (:address bsd-path)
  (#/removeItemWithObjectValue: (device-path-widget *hoppi-controller*) bsd-path)
  (ensure-serial-stream-closed (ccl::lisp-string-from-nsstring bsd-path)))

;;; Serial stream opening/closing routines
;;;
(defun close-serial-stream ()
  (when *serial-handle*
    (#/release *serial-handle*)
    (setf *serial-handle* nil))
  (when *serial-stream*
    (close *serial-stream*)
    (setf *serial-stream* nil))
  (when *serial-device*
    (setf *serial-device* nil)
    (report-connection-status "Serial device closed")))

(defun open-serial-stream (device-path)
  (when *serial-handle*
    (#/release *serial-handle*)
    (setf *serial-handle* nil))
  (when *serial-stream*
    (close *serial-stream*)
    (setf *serial-stream* nil))
  (setf *serial-device* device-path)
  (handler-case
      (progn
        (setf *serial-stream* (ccl::make-serial-stream device-path))
        (dbg-msg "Opened serial stream")
        (ccl::setup-serial-device (ccl::stream-device *serial-stream* :io)
                                  :baud-rate 9600 :parity :none :char-bits 8
                                  :stop-bits 1 :flow-control :none)
        (dbg-msg "Configured serial stream")
        (setf *serial-handle* (let ((fd (ccl::stream-device *serial-stream* :io)))
                                (#/initWithFileDescriptor: (#/alloc ns:ns-file-handle) fd)))
        (dbg-msg "Obtained serial handle")
        (#/addObserver:selector:name:object: (#/defaultCenter ns:ns-notification-center)
                                             *serial-controller*
                                             (ccl::@selector "serialStreamService:")
                                             #@"NSFileHandleDataAvailableNotification"
                                             *serial-handle*)
        (dbg-msg "Registered NSFileHandleDataAvailableNotification observer")
        (report-connection-status "Serial device open")
        (setf (serial-device *user-defaults*) device-path)
        (invoke-serial-async-read))
    (t () (progn
            (dbg-msg "Error: cannot open serial device")
            (setf *serial-device* nil)
            (setf *serial-stream* nil)
            (setf *serial-handle* nil)))))

(defun invoke-serial-async-read ()
  (#/waitForDataInBackgroundAndNotify *serial-handle*))

(defun ensure-serial-stream-closed (device-path)
  (when (equal device-path *serial-device*)
    (close-serial-stream)))
  
(defun switch-to-serial-stream (device-path)
  (unless (equal device-path *serial-device*)
    (close-serial-stream)
    (when (eql (connection-type *user-defaults*) :serial)
      (open-serial-stream device-path))))

;;; Serial stream controller -- a Cocoa class for serial stream events
;;;
(defclass serial-controller (ns:ns-object)
  ()
  (:metaclass ns:+ns-object))

(defmethod initialize-instance :after ((self serial-controller) &key &allow-other-keys)
  (#/addObserver:selector:name:object: (#/defaultCenter ns:ns-notification-center)
                                       self
                                       (ccl::@selector "serialDeviceSelectionChanged:")
                                       #@"NSComboBoxSelectionDidChangeNotification"
                                       (device-path-widget *hoppi-controller*))
  (#/addObserver:selector:name:object: (#/defaultCenter ns:ns-notification-center)
                                       self
                                       (ccl::@selector "serialDeviceTextChanged:")
                                       #@"NSControlTextDidChangeNotification"
                                       (device-path-widget *hoppi-controller*)))

(objc:defmethod (#/serialDeviceTextChanged: :void) ((self serial-controller) (note :id))
  ;; method invoked in response to a NSControlTextDidChangeNotification from (device-path-widget *hoppi-controller*)
  (declare (ignore note))
  (dbg-msg "serialDeviceTextChanged:")
  (let ((obj (#/objectValue (device-path-widget *hoppi-controller*))))
    (when (typep obj ns:ns-string)
      (switch-to-serial-stream (ccl::lisp-string-from-nsstring obj)))))

(objc:defmethod (#/serialDeviceSelectionChanged: :void) ((self serial-controller) (note :id))
  ;; method invoked in response to a NSComboBoxSelectionDidChangeNotification from (device-path-widget *hoppi-controller*)
  (declare (ignore note))
  (dbg-msg "serialDeviceSelectionChanged:")
  (let ((obj (#/objectValueOfSelectedItem (device-path-widget *hoppi-controller*))))
    (when (typep obj ns:ns-string)
      (switch-to-serial-stream (ccl::lisp-string-from-nsstring obj)))))

(objc:defmethod (#/serialStreamService: :void) ((self serial-controller) (note :id))
  ;; method invoked in response to a NSFileHandleDataAvailableNotification from *serial-handle*
  (declare (ignore note))
  (dbg-msg "serialStreamService:")
  (handler-case
      (when *serial-stream*
        (loop :while (listen *serial-stream*)
              :do (push-byte *krxbuf* (read-byte *serial-stream*)))
        (loop :while (packet-available-p *krxbuf*)
              :do (let ((pkt (pop-packet *krxbuf*)))
                    (push-packet *kpc* pkt)))
        (loop :while (byte-available-p *ktxbuf*)
              :do (let ((byte (pop-byte *ktxbuf*)))
                    (write-byte byte *serial-stream*)))
        (force-output *serial-stream*)
        (invoke-serial-async-read))
    (t () nil)))

;;; Utility functions
;;;
(defun load-nibfile (path owner)
  (let* ((filename (objc:make-nsstring path))  ; filename is owned by us
         (arr (#/arrayWithCapacity: ns:ns-mutable-array 32))  ; arr is autoreleased
         (dict (#/dictionaryWithObjectsAndKeys: ns:ns-mutable-dictionary  ; dict is autoreleased
                                                owner #&NSNibOwner
                                                arr #&NSNibTopLevelObjects
                                                ccl:+null-ptr+))
         (top-objs nil)
         (result (#/loadNibFile:externalNameTable:withZone: ns:ns-bundle
                                                            filename
                                                            dict
                                                            (#/zone #&NSApp))))
    (when result
      (dotimes (n (#/count arr))
        (let ((obj (#/objectAtIndex: arr n)))
          (setf top-objs (cons obj top-objs))))
      (dolist (obj top-objs)
          (#/retain obj)))
    (#/release filename)
    (values top-objs result)))

;;; Hoppi user defaults
;;;
(defclass user-defaults ()
  ((defaults        :initform nil)
   (current-path    :initform nil)
   (allow-overwrite :initform nil)
   (connection-type :initform nil)
   (serial-device   :initform nil)))

(defmethod initialize-instance :after ((ud user-defaults) &key &allow-other-keys)
  (with-slots (defaults) ud
    (setf defaults (#/standardUserDefaults ns:ns-user-defaults))
    (let ((dict (#/dictionary ns:ns-mutable-dictionary))
          (ns-path-str (objc:make-nsstring (namestring-home-folder)))
          (ns-ovrw-num (#/numberWithBool: ns:ns-number #$NO))
          (ns-conn-str #@"USB")
          (ns-devc-str #@""))
      (#/autorelease ns-path-str)
      (#/autorelease ns-devc-str)
      (#/setObject:forKey: dict ns-path-str #@"HoppiCurrentPath")
      (#/setObject:forKey: dict ns-ovrw-num #@"HoppiAllowOverwrite")
      (#/setObject:forKey: dict ns-conn-str #@"HoppiConnectionType")
      (#/setObject:forKey: dict ns-devc-str #@"HoppiSerialDevice")
      (#/registerDefaults: defaults dict))))

(defmethod current-path ((ud user-defaults))
    "Get the current-path as a Lisp string."
  (with-slots (defaults current-path) ud
    (unless current-path
      (let ((obj (#/stringForKey: defaults #@"HoppiCurrentPath")))
        (setf current-path (ccl::lisp-string-from-nsstring obj))))
    current-path))

(defmethod (setf current-path) (new-path (ud user-defaults))
  "Set the current-path, with NEW-PATH a Lisp namestring."
  (with-slots (defaults current-path) ud
    (let ((ns-str (objc:make-nsstring new-path)))
      (#/autorelease ns-str)
      (#/setObject:forKey: defaults ns-str #@"HoppiCurrentPath"))
    (setf current-path new-path)))

(defmethod allow-overwrite ((ud user-defaults))
  "Get the allow-overwrite setting from user defaults, as a Lisp boolean."
  ;; CCL does automatic Lisp bool to ObjC bool conversion here
  (with-slots (defaults) ud
    (#/boolForKey: defaults #@"HoppiAllowOverwrite")))

(defmethod (setf allow-overwrite) (allowed (ud user-defaults))
  "Set the allow-overwrite user default, with ALLOWED as a Lisp boolean."
  ;; CCL does automatic Lisp bool to ObjC bool conversion here
  (with-slots (defaults) ud
    (#/setBool:forKey: defaults allowed #@"HoppiAllowOverwrite")))

(defmethod connection-type ((ud user-defaults))
  "Get the connection-type as a Lisp keyword symbol."
  (with-slots (defaults connection-type) ud
      (unless connection-type
        (let* ((obj (#/stringForKey: defaults #@"HoppiConnectionType"))
               (val (ccl::lisp-string-from-nsstring obj)))
          (setf connection-type (intern (string-upcase val) :keyword))))
    connection-type))

(defmethod (setf connection-type) (conn-type (ud user-defaults))
  "Set the connection-type, with CONN-TYPE a Lisp keyword."
  (with-slots (defaults connection-type) ud
    (let ((ns-str (objc:make-nsstring (string conn-type))))
      (#/autorelease ns-str)
      (#/setObject:forKey: defaults ns-str #@"HoppiConnectionType"))
    (setf connection-type conn-type)))

(defmethod serial-device ((ud user-defaults))
  "Get the serial-device as a Lisp string."
  (with-slots (defaults serial-device) ud
    (unless serial-device
      (let ((obj (#/stringForKey: defaults #@"HoppiSerialDevice")))
        (setf serial-device (ccl::lisp-string-from-nsstring obj))))
    serial-device))

(defmethod (setf serial-device) (new-device (ud user-defaults))
  "Set the serial-device, with NEW-DEVICE a Lisp namestring."
  (with-slots (defaults serial-device) ud
    (let ((ns-str (objc:make-nsstring new-device)))
      (#/autorelease ns-str)
      (#/setObject:forKey: defaults ns-str #@"HoppiSerialDevice"))
    (setf serial-device new-device)))

;;; Hoppi Application class -- a Cocoa class
;;;
(defclass hoppi-application (ns:ns-application)
  ((show-packet-logger-widget     :foreign-type :id :accessor show-packet-logger-widget)
   (show-message-logger-widget    :foreign-type :id :accessor show-message-logger-widget)
   (show-grob-viewer-widget       :foreign-type :id :accessor show-grob-viewer-widget)
   (receive-file-as-string-widget :foreign-type :id :accessor receive-file-as-string-widget))
  (:metaclass ns:+ns-object))

(objc:defmethod (#/hoppiControllerWillClose: :void) ((self hoppi-application) (sender :id))
  (declare (ignore sender))
  (#/terminate: self self))

(objc:defmethod (#/togglePacketLogger: :void) ((self hoppi-application) (sender :id))
  (cond ((null *packet-logger*)
         (setf *packet-logger* (make-instance 'packet-logger))
         (#/setTitle: sender #@"Hide Packet Logger"))
        (t
         (#/close (window-widget *packet-logger*))
         (setf *packet-logger* nil)
         (#/setTitle: sender #@"Show Packet Logger"))))

(objc:defmethod (#/toggleMessageLogger: :void) ((self hoppi-application) (sender :id))
  (cond ((null *message-logger*)
         (setf *message-logger* (make-instance 'message-logger))
         (#/setTitle: sender #@"Hide Message Logger"))
        (t
         (#/close (window-widget *message-logger*))
         (setf *message-logger* nil)
         (#/setTitle: sender #@"Show Message Logger"))))

(objc:defmethod (#/toggleGrobViewer: :void) ((self hoppi-application) (sender :id))
  (cond ((null *grob-viewer*)
         (setf *grob-viewer* (make-instance 'grob-viewer))
         (#/setTitle: sender #@"Hide GROB Viewer"))
        (t
         (#/close (window-widget *grob-viewer*))
         (setf *grob-viewer* nil)
         (#/setTitle: sender #@"Show GROB Viewer"))))

(objc:defmethod (#/sendFile: :void) ((self hoppi-application) (sender :id))
  (declare (ignore sender))
  (let ((ns-str (objc:make-nsstring (current-path *user-defaults*))))
    (#/autorelease ns-str)
    #-POWERPC (#/setDirectoryURL: *file-chooser* (#/fileURLWithPath:isDirectory: ns:ns-url ns-str #$YES))
    #+POWERPC (#/setDirectory: *file-chooser* ns-str)
    (#/setTitle: *file-chooser* #@"Choose File to Send")
    (#/setPrompt: *file-chooser* #@"Send"))
  (when (eql (#/runModal *file-chooser*) #$NSOKButton)
    (let ((ns-str (#/filename *file-chooser*)))
      (send-file *kpc* (ccl::lisp-string-from-nsstring ns-str)))))

(objc:defmethod (#/sendFileAsString: :void) ((self hoppi-application) (sender :id))
  (declare (ignore sender))
  (let ((ns-str (objc:make-nsstring (current-path *user-defaults*))))
    (#/autorelease ns-str)
    #-POWERPC (#/setDirectoryURL: *file-chooser* (#/fileURLWithPath:isDirectory: ns:ns-url ns-str #$YES))
    #+POWERPC (#/setDirectory: *file-chooser* ns-str)
    (#/setTitle: *file-chooser* #@"Choose File to Send as String")
    (#/setPrompt: *file-chooser* #@"Send as String"))
  (when (eql (#/runModal *file-chooser*) #$NSOKButton)
    (let ((ns-str (#/filename *file-chooser*)))
      (send-file *kpc* (ccl::lisp-string-from-nsstring ns-str) :text-as-string))))

(objc:defmethod (#/receiveFileAsString: :void) ((self hoppi-application) (sender :id))
  (if (eql (#/state sender) #$NSOffState)
      (setf (transfer-type *kpc*) :text-as-string)
      (setf (transfer-type *kpc*) nil)))

(objc:defmethod (#/revealInFinder: :void) ((self hoppi-application) (sender :id))
  (declare (ignore sender))
  (let ((ns-str (objc:make-nsstring (current-path *user-defaults*))))
    (#/autorelease ns-str)
    (#/selectFile:inFileViewerRootedAtPath: (#/sharedWorkspace ns:ns-workspace)
                                            ns-str
                                            #@"/")))

(objc:defmethod (#/packetLoggerWillClose: :void) ((self hoppi-application) (sender :id))
  (declare (ignore sender))
  (setf *packet-logger* nil)
  (#/setTitle: (show-packet-logger-widget *hoppi-application*) #@"Show Packet Logger"))

(objc:defmethod (#/messageLoggerWillClose: :void) ((self hoppi-application) (sender :id))
  (declare (ignore sender))
  (setf *message-logger* nil)
  (#/setTitle: (show-message-logger-widget *hoppi-application*) #@"Show Message Logger"))

(objc:defmethod (#/grobViewerWillClose: :void) ((self hoppi-application) (sender :id))
  (declare (ignore sender))
  (setf *grob-viewer* nil)
  (#/setTitle: (show-grob-viewer-widget *hoppi-application*) #@"Show GROB Viewer"))

;;; Hoppi Application Delegate class -- a Cocoa class
;;;
(defclass hoppi-application-delegate (ns:ns-object)
  ()
  (:metaclass ns:+ns-object))

(objc:defmethod (#/applicationDidFinishLaunching: :void) ((self hoppi-application-delegate) (note :id))
  (declare (ignore note))
  (hoppi-init ccl::*NSApp*))

;;; Hoppi Controller class -- a Cocoa class for the main window
;;;
(defclass hoppi-controller (ns:ns-object)
  ((window-widget            :foreign-type :id :accessor window-widget)
   (current-path-widget      :foreign-type :id :accessor current-path-widget)
   (allow-overwrite-widget   :foreign-type :id :accessor allow-overwrite-widget)
   (device-path-widget       :foreign-type :id :accessor device-path-widget)
   (connection-status-widget :foreign-type :id :accessor connection-status-widget)
   (connection-type-widget   :foreign-type :id :accessor connection-type-widget)
   (kermit-status-widget     :foreign-type :id :accessor kermit-status-widget)
   (kermit-reset-widget      :foreign-type :id :accessor kermit-reset-widget)
   (message-area-widget      :foreign-type :id :accessor message-area-widget)
   (retry-count-widget       :foreign-type :id :accessor retry-count-widget)
   (nib-objects :accessor nib-objects :initform nil))
  (:metaclass ns:+ns-object))

(defmethod initialize-instance :after ((self hoppi-controller) &key &allow-other-keys)
  (let ((nib-filename (if *nib-path*
                          (concatenate 'string *nib-path* "hoppi-controller.nib")
                          (ccl::lisp-string-from-nsstring (#/pathForResource:ofType: (#/mainBundle ns:ns-bundle) #@"hoppi-controller" #@"nib")))))
    (setf (nib-objects self) (load-nibfile nib-filename self))
    ;; listen for window-will-close notifications
    (#/addObserver:selector:name:object: (#/defaultCenter ns:ns-notification-center)
                                         *hoppi-application*
                                         (ccl::@selector "hoppiControllerWillClose:")
                                         #@"NSWindowWillCloseNotification"
                                         (window-widget self))
    ;; hook disposal of NIB objects into lisp GC, see ccl:terminate method following
    (ccl:terminate-when-unreachable self)))

(defmethod ccl:terminate ((self hoppi-controller))
  (dolist (top-obj (nib-objects self))
    (unless (eql top-obj ccl:+null-ptr+)
      (#/release top-obj))))

(objc:defmethod (#/allowOverwriteChanged: :void) ((self hoppi-controller) (sender :id))
  (declare (ignore sender))
  (let ((allowed (eql (#/state (allow-overwrite-widget self)) #$NSOnState)))
    (setf (allow-overwrite *user-defaults*) allowed)
    (setf (overwrite-action *kwspace*) (if allowed :supersede nil))))

(objc:defmethod (#/resetKermit: :void) ((self hoppi-controller) (sender :id))
  (declare (ignore sender))
  (reset-protocol *kpc*)
  (report-kermit-state (kermit-state-user-text (protocol-state *kpc*)))
  (report-kermit-retry-count (retry-count *kpc*))
  (report-kermit-message ""))

(objc:defmethod (#/oneSecondTick: :void) ((self hoppi-controller) (sender :id))
  (declare (ignore sender))
  (one-second-tick *kpc*))

(objc:defmethod (#/chooseFolder: :void) ((self hoppi-controller) (sender :id))
  (declare (ignore sender))
  (let ((ns-str (objc:make-nsstring (current-path *user-defaults*))))
    (#/autorelease ns-str)
    #-POWERPC (#/setDirectoryURL: *folder-chooser* (#/fileURLWithPath:isDirectory: ns:ns-url ns-str #$YES))
    #+POWERPC (#/setDirectory: *folder-chooser* ns-str))
  (when (eql (#/runModal *folder-chooser*) #$NSOKButton)
    (let* ((ns-str (#/filename *folder-chooser*))
           (new-path (concatenate 'string (ccl::lisp-string-from-nsstring ns-str) "/")))
      (setf (current-folder-path *kwspace*) new-path))))

(objc:defmethod (#/chooseConnectionType: :void) ((self hoppi-controller) (sender :id))
  (declare (ignore sender))
  (cond ((and (eql (#/selectedSegment (connection-type-widget self)) 0)
              (not (eql (connection-type *user-defaults*) :usb)))
         ;; switch to USB
         (close-serial-stream)
         (ccl:external-call "teardown_serial_io")         
         (#/removeAllItems (device-path-widget self))
         (#/setStringValue: (device-path-widget self) #@"")
         (#/setEnabled: (device-path-widget self) #$NO)
         (setf (connection-type *user-defaults*) :usb)
         (ccl:external-call "setup_usb_io" :address usb-post-configure-callback :address usb-post-unconfigure-callback :int))
        ((and (eql (#/selectedSegment (connection-type-widget self)) 1)
              (not (eql (connection-type *user-defaults*) :serial)))
         ;; switch to serial
         (ccl:external-call "teardown_usb_io")         
         (#/setEnabled: (device-path-widget self) #$YES)
         (clear-connection-status)
         (setf (connection-type *user-defaults*) :serial)
         (ccl:external-call "setup_serial_io" :address serial-arrivals-callback :address serial-departures-callback :int))))

;;; Packet Logger class -- a Cocoa class for the packet logger window
;;;
(defclass packet-logger (ns:ns-object)
  ((window-widget         :foreign-type :id :accessor window-widget)
   (show-hex-widget       :foreign-type :id :accessor show-hex-widget)
   (log-transcript-widget :foreign-type :id :accessor log-transcript-widget)
   (nib-objects :accessor nib-objects :initform nil))
  (:metaclass ns:+ns-object))

(defmethod initialize-instance :after ((self packet-logger) &key &allow-other-keys)
  (setf *packet-logger* self)
  (let ((nib-filename (if *nib-path*
                          (concatenate 'string *nib-path* "packet-logger.nib")
                          (ccl::lisp-string-from-nsstring (#/pathForResource:ofType: (#/mainBundle ns:ns-bundle) #@"packet-logger" #@"nib")))))
    (setf (nib-objects self) (load-nibfile nib-filename self))
    (#/makeKeyWindow (window-widget self))
    ;; listen for window-will-close notifications
    (#/addObserver:selector:name:object: (#/defaultCenter ns:ns-notification-center)
                                         *hoppi-application*
                                         (ccl::@selector "packetLoggerWillClose:")
                                         #@"NSWindowWillCloseNotification"
                                         (window-widget self))
    ;; hook disposal of NIB objects into lisp GC, see ccl:terminate method following
    (ccl:terminate-when-unreachable self)))

(defmethod ccl:terminate ((self packet-logger))
  (dolist (top-obj (nib-objects self))
    (unless (eql top-obj ccl:+null-ptr+)
      (#/release top-obj))))

(objc:defmethod (#/clearLog: :void) ((self packet-logger) (sender :id))
  (declare (ignore sender))
  (when *packet-logger*
    (let* ((ns-str (objc:make-nsstring ""))
           (ns-tv (log-transcript-widget *packet-logger*)))
      (#/autorelease ns-str)
      (#/setString: ns-tv ns-str)
      ;;(#/scrollRangeToVisible: ns-tv (ns:make-ns-range (#/length (#/string ns-tv)) 0))
      (#/setNeedsDisplay: ns-tv #$YES))))

;;; Message Logger class -- a Cocoa class for the message logger window
;;;
(defclass message-logger (ns:ns-object)
  ((window-widget         :foreign-type :id :accessor window-widget)
   (log-transcript-widget :foreign-type :id :accessor log-transcript-widget)
   (nib-objects :accessor nib-objects :initform nil))
  (:metaclass ns:+ns-object))

(defmethod initialize-instance :after ((self message-logger) &key &allow-other-keys)
  (setf *message-logger* self)
  (let ((nib-filename (if *nib-path*
                          (concatenate 'string *nib-path* "message-logger.nib")
                          (ccl::lisp-string-from-nsstring (#/pathForResource:ofType: (#/mainBundle ns:ns-bundle) #@"message-logger" #@"nib")))))
    (setf (nib-objects self) (load-nibfile nib-filename self))
    (#/makeKeyWindow (window-widget self))
    ;; listen for window-will-close notifications
    (#/addObserver:selector:name:object: (#/defaultCenter ns:ns-notification-center)
                                         *hoppi-application*
                                         (ccl::@selector "messageLoggerWillClose:")
                                         #@"NSWindowWillCloseNotification"
                                         (window-widget self))
    ;; hook disposal of NIB objects into lisp GC, see ccl:terminate method following
    (ccl:terminate-when-unreachable self)))

(defmethod ccl:terminate ((self message-logger))
  (dolist (top-obj (nib-objects self))
    (unless (eql top-obj ccl:+null-ptr+)
      (#/release top-obj))))

(objc:defmethod (#/clearLog: :void) ((self message-logger) (sender :id))
  (declare (ignore sender))
  (when *message-logger*
    (let* ((ns-str (objc:make-nsstring ""))
           (ns-tv (log-transcript-widget *message-logger*)))
      (#/autorelease ns-str)
      (#/setString: ns-tv ns-str)
      ;;(#/scrollRangeToVisible: ns-tv (ns:make-ns-range (#/length (#/string ns-tv)) 0))
      (#/setNeedsDisplay: ns-tv #$YES))))

;;; GROB Viewer class -- a Cocoa class for the GROB viewer window
;;;
(defclass grob-viewer (ns:ns-object)
  ((window-widget       :foreign-type :id :accessor window-widget)
   (grob-view-widget    :foreign-type :id :accessor grob-view-widget)
   (grob-info-widget    :foreign-type :id :accessor grob-info-widget)
   (scale-slider-widget :foreign-type :id :accessor scale-slider-widget)
   (scale-info-widget   :foreign-type :id :accessor scale-info-widget)
   (current-scale       :initform 200.0   :accessor current-scale)
   (nib-objects         :initform nil     :accessor nib-objects))
  (:metaclass ns:+ns-object))

(defmethod initialize-instance :after ((self grob-viewer) &key &allow-other-keys)
  (setf *grob-viewer* self)
  (let ((nib-filename (if *nib-path*
                          (concatenate 'string *nib-path* "grob-viewer.nib")
                          (ccl::lisp-string-from-nsstring (#/pathForResource:ofType: (#/mainBundle ns:ns-bundle) #@"grob-viewer" #@"nib")))))
    (setf (nib-objects self) (load-nibfile nib-filename self))
    (#/makeKeyWindow (window-widget self))
    ;; listen for window-will-close notifications
    (#/addObserver:selector:name:object: (#/defaultCenter ns:ns-notification-center)
                                         *hoppi-application*
                                         (ccl::@selector "grobViewerWillClose:")
                                         #@"NSWindowWillCloseNotification"
                                         (window-widget self))
    ;; hook disposal of NIB objects into lisp GC, see ccl:terminate method following
    (ccl:terminate-when-unreachable self)))

(defmethod ccl:terminate ((self grob-viewer))
  (dolist (top-obj (nib-objects self))
    (unless (eql top-obj ccl:+null-ptr+)
      (#/release top-obj))))

(objc:defmethod (#/scaleSliderChanged: :void) ((self grob-viewer) (sender :id))
  (when *grob-viewer*
    (let ((scale (#/floatValue sender)))
      (setf (current-scale *grob-viewer*) scale)
      (let ((ns-str (objc:make-nsstring (format nil "~A%" (floor scale)))))
        (#/autorelease ns-str)
        (#/setStringValue: (scale-info-widget *grob-viewer*) ns-str))
      (#/setNeedsDisplay: (grob-view-widget *grob-viewer*) #$YES))))

(defclass hoppi-grob-view (ns:ns-view)
  ()
  (:metaclass ns:+ns-object)
  (:documentation "An NSView subclass."))

(objc:defmethod (#/acceptsFirstResponder :<BOOL>) ((self hoppi-grob-view))
  #$YES)

(objc:defmethod (#/drawRect: :void) ((self hoppi-grob-view) (dirty-rect :<NSR>ect))
  (when (and *grob-viewer* *grob-array*)
    (let* ((zoom (/ (current-scale *grob-viewer*) 100.0))
           (cols (array-dimension *grob-array* 1))
           (rows (array-dimension *grob-array* 0))
           (bg-width (* zoom cols))
           (bg-height (* zoom rows))
           (dot-width (* zoom 1.0))
           (dot-height (* zoom 1.0)))
      (objc:send (grob-view-widget *grob-viewer*)
                 :set-frame-size (ccl::ns-make-size (float (* (/ (current-scale *grob-viewer*) 100.0) cols) +cgfloat+)
                                                    (float (* (/ (current-scale *grob-viewer*) 100.0) rows) +cgfloat+)))
      (let ((white (objc:send (find-class 'ns:ns-color) 'white-color))
            (black (objc:send (find-class 'ns:ns-color) 'black-color)))
        ;; Fill background
        (objc:send white 'set)
        (objc:send (find-class 'ns:ns-bezier-path)
                   :fill-rect (ccl::ns-make-rect (float 0 +cgfloat+)
                                                 (float 0 +cgfloat+)
                                                 (float bg-width +cgfloat+)
                                                 (float bg-height +cgfloat+)))
        ;; Draw GROB dots
        (objc:send black 'set)
        (dotimes (y rows)
          (dotimes (x cols)
            (if (eql (aref *grob-array* y x) 1)
                (objc:send (find-class 'ns:ns-bezier-path)
                           :fill-rect (ccl::ns-make-rect (float (* zoom x)            +cgfloat+)
                                                         (float (* zoom (- rows y 1)) +cgfloat+)
                                                         (float dot-width  +cgfloat+)
                                                         (float dot-height +cgfloat+))))))))))

;; The following copy: method crashes to the kernel debugger in CCL1.6 on OSX10.5 PowerPC -- don't understand why :-(
#-:POWERPC
(objc:defmethod (#/copy: :void) ((self hoppi-grob-view) (sender :id))
  (when (and *grob-viewer* *grob-array*)
    (let ((context (objc:send (find-class 'ns:ns-graphics-context) 'current-context))
          (pboard (objc:send (find-class 'ns:ns-pasteboard) 'general-pasteboard))
          (bitmap (objc:send (find-class 'ns:ns-bitmap-image-rep) 'alloc))
          (scale (floor (/ (current-scale *grob-viewer*) 100))))
      (setf bitmap (objc:send bitmap :init-with-bitmap-data-planes ccl:+null-ptr+
                                     :pixels-wide (* scale (array-dimension *grob-array* 1))
                                     :pixels-high (* scale (array-dimension *grob-array* 0))
                                     :bits-per-sample 8
                                     :samples-per-pixel 4
                                     :has-alpha #$YES
                                     :is-planar #$NO
                                     :color-space-name #$NSCalibratedRGBColorSpace
                                     :bytes-per-row 0
                                     :bits-per-pixel 0))
      (let ((bitmap-context (objc:send (find-class 'ns:ns-graphics-context)
                                       :graphics-context-with-bitmap-image-rep bitmap)))
        (objc:send (find-class 'ns:ns-graphics-context)
                   :set-current-context bitmap-context)
        (objc:send self :draw-rect (ns:make-ns-rect (float 0 +cgfloat+)
                                                    (float 0 +cgfloat+)
                                                    (float (* scale (array-dimension *grob-array* 1)) +cgfloat+)
                                                    (float (* scale (array-dimension *grob-array* 0)) +cgfloat+)))
        (objc:send pboard :declare-types (make-instance 'ns:ns-array :array-with-object #$NSTIFFPboardType)
                          :owner self)
        (objc:send pboard :set-data (#/TIFFRepresentation bitmap) :for-type #$NSTIFFPboardType)
        (objc:send (find-class 'ns:ns-graphics-context)
                   :set-current-context context)
        (#/release bitmap)))))

;;; Status updates to show in the GUI
;;;
(defun clear-connection-status ()
  (#/setStringValue: (connection-status-widget *hoppi-controller*) #@""))

(defun report-connection-status (msg)
  (let ((ns-str (objc:make-nsstring msg)))
    (#/autorelease ns-str)
    (#/setStringValue: (connection-status-widget *hoppi-controller*) ns-str)))

(defun report-kermit-state (msg)
  (let ((ns-str (objc:make-nsstring msg)))
    (#/autorelease ns-str)
    (#/setStringValue: (kermit-status-widget *hoppi-controller*) ns-str)))

(defun report-kermit-retry-count (count)
  (let ((ns-str (objc:make-nsstring (if (eql count 0)
                                        ""
                                        (format nil "Retry ~A" count)))))
    (#/autorelease ns-str)
    (#/setStringValue: (retry-count-widget *hoppi-controller*) ns-str)))

(defun report-kermit-message (msg)
  (let ((ns-str (objc:make-nsstring (format nil "~A" msg))))
    (#/autorelease ns-str)
    (#/setStringValue: (message-area-widget *hoppi-controller*) ns-str)))

(defun report-rx-packet (seq-num type-char data checksum chk-passed)
  (when *packet-logger*
    (let* ((ns-str (objc:make-nsstring
                    (concatenate 'string
                                 (format nil "↪ ~2D ~A〖~{~A~}〗〖~{~A~}〗~A~%"
                                         seq-num type-char
                                         (loop :for b :across data :collect (hp-code-to-unicode-char b))
                                         (loop :for b :across checksum :collect (hp-code-to-unicode-char b))
                                         (if chk-passed "" "BAD CHECKSUM"))
                                 (if (eql (#/state (show-hex-widget *packet-logger*)) #$NSOnState)
                                     (format nil "      [~{ ~2,'0X~} ]  [~{ ~2,'0X~} ]~%"
                                             (coerce data 'list)
                                             (coerce checksum 'list))
                                     ""))))
           (ns-tv (log-transcript-widget *packet-logger*))
           (ns-ts (#/textStorage ns-tv)))
      (#/autorelease ns-str)
      (#/replaceCharactersInRange:withString: ns-ts
                                              (ns:make-ns-range (#/length ns-ts) 0)
                                              ns-str)
      (#/scrollRangeToVisible: ns-tv (ns:make-ns-range (#/length (#/string ns-tv)) 0))
      (#/setNeedsDisplay: ns-tv #$YES))))

(defun report-tx-packet (seq-num type-char data checksum)
  (when *packet-logger*
    (let* ((ns-str (objc:make-nsstring
                    (concatenate 'string
                                 (format nil "↩ ~2D ~A〖~{~A~}〗〖~{~A~}〗~%"
                                         seq-num type-char
                                         (loop :for b :across data :collect (hp-code-to-unicode-char b))
                                         (loop :for b :across checksum :collect (hp-code-to-unicode-char b)))
                                 (if (eql (#/state (show-hex-widget *packet-logger*)) #$NSOnState)
                                     (format nil "      [~{ ~2,'0X~} ]  [~{ ~2,'0X~} ]~%"
                                             (coerce data 'list)
                                             (coerce checksum 'list))))))
           (ns-tv (log-transcript-widget *packet-logger*))
           (ns-ts (#/textStorage ns-tv)))
      (#/autorelease ns-str)
      (#/replaceCharactersInRange:withString: ns-ts
                                              (ns:make-ns-range (#/length ns-ts) 0)
                                              ns-str)
      (#/scrollRangeToVisible: ns-tv (ns:make-ns-range (#/length (#/string ns-tv)) 0))
      (#/setNeedsDisplay: ns-tv #$YES))))

(defun report-rx-message (title message)
  (when *message-logger*
    (let* ((ns-str (objc:make-nsstring (format nil "↪ ~A~%~A~%" title message)))
           (ns-tv (log-transcript-widget *message-logger*))
           (ns-ts (#/textStorage ns-tv)))
      (#/autorelease ns-str)
      (#/replaceCharactersInRange:withString: ns-ts
                                              (ns:make-ns-range (#/length ns-ts) 0)
                                              ns-str)
      (#/scrollRangeToVisible: ns-tv (ns:make-ns-range (#/length (#/string ns-tv)) 0))
      (#/setNeedsDisplay: ns-tv #$YES))))

(defun report-tx-message (title message)
  (when *message-logger*
    (let* ((ns-str (objc:make-nsstring (format nil "↩ ~A~%~A~%" title message)))
           (ns-tv (log-transcript-widget *message-logger*))
           (ns-ts (#/textStorage ns-tv)))
      (#/autorelease ns-str)
      (#/replaceCharactersInRange:withString: ns-ts
                                              (ns:make-ns-range (#/length ns-ts) 0)
                                              ns-str)
      (#/scrollRangeToVisible: ns-tv (ns:make-ns-range (#/length (#/string ns-tv)) 0))
      (#/setNeedsDisplay: ns-tv #$YES))))

(defun parse-grob-message (message)
  "Parse a GROB message, return a 2D bit array representing its contents.
If the message is malformed or not a GROB message then return NIL."
  (let ((grob-start (search "GROB" message)))
    (when grob-start
      (multiple-value-bind (width width-end)
          (parse-integer message :start (+ grob-start 4) :junk-allowed t)
        (when width
          (multiple-value-bind (height height-end)
              (parse-integer message :start width-end :junk-allowed t)
            (when height
              (let ((nibbles-per-row (* (ceiling width 8) 2)))
                (setf width (mod width 1000))
                (setf height (mod height 1000))
                (let ((arr (make-array (list height width) :element-type 'bit :initial-element 0)))
                  (loop :for c :across (subseq message (1+ height-end))
                     :for n :from 0
                     :do (let* ((cc (char-code c))
                                (nibble (mod (if (< cc 58) (- cc 48) (- cc 55)) 16)))
                           (multiple-value-bind (row col) (floor n nibbles-per-row)
                             (when (< row height)
                               (dotimes (j 4)
                                 (when (< (+ (* 4 col) j) width)
                                   (setf (aref arr row (+ (* 4 col) j))
                                         (ldb (byte 1 j) nibble))))))))
                  arr)))))))))

(defun view-rx-message (title message)
  "View a received message.
Currently only messages containing GROB data are supported."
  (declare (ignore title))
  (let ((arr (parse-grob-message message)))
    (when arr
      (unless *grob-viewer*
        (#/toggleGrobViewer: *hoppi-application* (show-grob-viewer-widget *hoppi-application*)))
      (when *grob-viewer*
        (setf *grob-array* arr)
        (let ((ns-str (objc:make-nsstring (format nil "~{~A~^ by ~}" (reverse (array-dimensions *grob-array*))))))
          (#/autorelease ns-str)
          (#/setStringValue: (grob-info-widget *grob-viewer*) ns-str))
        (#/setNeedsDisplay: (grob-view-widget *grob-viewer*) #$YES)))))

;;; Link internal Kermit events to GUI status updates
;;;
(defparameter *io-msg* "")
(defparameter *io-name* "")
(defparameter *io-bytes* 0)

(defun kermit-state-user-text (state)
  "Map the kermit-protocol-controller's states into more user friendly terms."
  (case state
    ((:receive-server-idle)  "Server idle")
    ((:receive-init)         "Initializing incoming connection")
    ((:receive-file)         "Receiving file name")
    ((:receive-data)         "Receiving file data")
    ((:send-init)            "Initializing outgoing connection")
    ((:send-init-ack)        "Initializing outgoing connection")
    ((:open-file)            "Opening outgoing file")
    ((:send-file)            "Sending file name")
    ((:send-file-ack)        "Sending file name")
    ((:send-data)            "Sending file data")
    ((:send-data-ack)        "Sending file data")
    ((:send-eof)             "Sending end-of-file")
    ((:send-eof-ack)         "Sending end-of-file")
    ((:send-break)           "Terminating outgoing connection")
    ((:send-break-ack)       "Terminating outgoing connection")
    ((:send-server-init)     "Initializing server response")
    ((:send-server-init-ack) "Initializing server response")
    ((:send-gen-cmd)         "Sending server response")
    ((:send-gen-cmd-ack)     "Sending server response")
    ((:complete)             "Transaction complete")
    ((:abort)                "Transaction aborting")
    ((:restart)              "Kermit protocol restarting")
    ((:restart-now)          "Kermit protocol restarting")
    (t                       "*** INTERNAL ERROR ***")))

(defmethod run-protocol :after ((kpc kermit-protocol-controller) &key &allow-other-keys)
  (when (eql kpc *kpc*)
    (report-kermit-state (kermit-state-user-text (protocol-state kpc)))
    (report-kermit-retry-count (retry-count kpc))))

(defmethod run-protocol-step :after ((kpc kermit-protocol-controller) &key &allow-other-keys)
  (when (eql kpc *kpc*)
    (cond ((eql (protocol-state kpc) :complete)
           (report-kermit-message (format nil "~A ~A~%Bytes: ~D~%Done" *io-msg* *io-name* *io-bytes*)))
          ((eql (protocol-state kpc) :abort)
           (report-kermit-message (format nil "~A ~A~%Bytes: ~D~%ABORTED" *io-msg* *io-name* *io-bytes*))))))

(defmethod (setf tx-error-message) :after (msg (kpc kermit-protocol-controller))
  (when (eql kpc *kpc*)
    (setf *io-msg* (format nil "ERROR: ~A" msg))
    (report-kermit-message (format nil "~A~%" *io-msg*))))

(defmethod (setf rx-error-message) :after (msg (kpc kermit-protocol-controller))
  (when (eql kpc *kpc*)
    (setf *io-msg* (format nil "Received ERROR message: ~A" msg))
    (report-kermit-message (format nil "~A~%" *io-msg*))))

(defmethod (setf transfer-type) :after (val (kpc kermit-protocol-controller))
  (when (eql kpc *kpc*)
    (if (eql val :text-as-string)
        (#/setState: (receive-file-as-string-widget *hoppi-application*) #$NSOnState)
        (#/setState: (receive-file-as-string-widget *hoppi-application*) #$NSOffState))))

(defmethod hoppi-kermit::process-s-packet :after ((kpc kermit-protocol-controller) (pkt kermit-packet))
  (when (eql kpc *kpc*)
    (setf *io-msg* "")
    (setf *io-name* "")
    (setf *io-bytes* 0)
    (report-kermit-message (format nil ""))))

(defmethod hoppi-kermit::process-i-packet :after ((kpc kermit-protocol-controller) (pkt kermit-packet))
  (when (eql kpc *kpc*)
    (setf *io-msg* "")
    (setf *io-name* "")
    (setf *io-bytes* 0)
    (report-kermit-message (format nil ""))))

(defmethod open-file-for-writing :after ((kws kermit-workspace) path &optional transfer-type)
  (declare (ignore path))
  (when (eql kws *kwspace*)
    (let ((actual-path (current-output-name kws)))
      (when actual-path
        (setf *io-msg* (if (eql transfer-type :text-as-string)
                           "Receiving file as string:"
                           "Receiving file:"))
        (setf *io-name* (namestring-file-part actual-path))
        (setf *io-bytes* 0)
        (report-kermit-message (format nil "~A ~A" *io-msg* *io-name*))))))

(defmethod open-message-for-writing :after ((kws kermit-workspace) title &optional transfer-type)
  (when (eql kws *kwspace*)
    (setf *io-msg* (if (eql transfer-type :text-as-string)
                       "Receiving message as string:"
                       "Receiving message:"))
    (setf *io-name* title)
    (setf *io-bytes* 0)
    (report-kermit-message (format nil "~A ~A" *io-msg* *io-name*))))

(defmethod write-to-current-output-stream :after ((kws kermit-workspace) bytes)
  (when (eql kws *kwspace*)
    (incf *io-bytes* (length bytes))
    (report-kermit-message (format nil "~A ~A~%Bytes: ~D" *io-msg* *io-name* *io-bytes*))))

(defmethod close-current-output-stream :before ((kws kermit-workspace))
  (when (eql kws *kwspace*)
    (when (and (eql (current-output-type kws) :message)
               (current-output-stream kws))
      (let* ((bytes (ccl:get-output-stream-vector (current-output-stream kws)))
             (title (current-output-name kws))
             (msg (if (eql (transfer-type kws) :binary)
                      (apply #'concatenate 'string
                             (map 'list (lambda (x) (format nil " ~2,'0X" x)) bytes))
                      (map 'string #'hp-code-to-unicode-char bytes))))
        (report-rx-message title msg)
        (view-rx-message title msg)))))

(defmethod open-file-for-reading :after ((kws kermit-workspace) path &optional transfer-type)
  (when (eql kws *kwspace*)
    (setf *io-msg* (if (eql transfer-type :text-as-string)
                     "Sending file as string:"
                     "Sending file:"))
    (setf *io-name* path)
    (setf *io-bytes* 0)
    (report-kermit-message (format nil "~A ~A" *io-msg* *io-name*))))

(defmethod open-message-for-reading :after ((kws kermit-workspace) title msg &optional transfer-type)
  (when (eql kws *kwspace*)
    (setf *io-msg* (if (eql transfer-type :text-as-string)
                     "Sending message as string:"
                     "Sending message:"))
    (setf *io-name* title)
    (setf *io-bytes* 0)
    (report-kermit-message (format nil "~A ~A" *io-msg* *io-name*))
    (report-tx-message title msg)))

(defmethod hoppi-kermit::send-d-packet :after ((kpc kermit-protocol-controller))
  (setf *io-bytes* (num-user-bytes-sent kpc))
  (report-kermit-message (format nil "~A ~A~%Bytes: ~D" *io-msg* *io-name* *io-bytes*)))

(defmethod (setf current-folder-path) :after (new-path (kws kermit-workspace))
  (when (eql kws *kwspace*)
    (let ((ns-path-str (objc:make-nsstring new-path)))
      (#/autorelease ns-path-str)
      (#/setURL: (current-path-widget *hoppi-controller*)
                 (#/fileURLWithPath:isDirectory: ns:ns-url ns-path-str #$YES)))
    (setf (current-path *user-defaults*) new-path)))

(defmethod (setf overwrite-action) :after (action (kws kermit-workspace))
  (when (eql kws *kwspace*)
    (#/setState: (allow-overwrite-widget *hoppi-controller*) (if action #$NSOnState #$NSOffState))
    (setf (allow-overwrite *user-defaults*) (if action t nil))))

(defmethod push-packet :after ((kbuf kermit-tx-buffer) (pkt kermit-packet))
  (when (eql kbuf *ktxbuf*)
    (let ((chk-size (checksum-size (current-parameters *kpc*))))
      (report-tx-packet (packet-seq pkt) (code-char (packet-type pkt)) (packet-data pkt chk-size) (packet-checksum pkt chk-size)))))

(defmethod verify-checksum-p :around ((pkt kermit-packet) checksum-size)
  (declare (ignore checksum-size))
  (let ((chk-passed (call-next-method))
        (chk-size (checksum-size (current-parameters *kpc*))))
    (report-rx-packet (packet-seq pkt) (code-char (packet-type pkt)) (packet-data pkt chk-size) (packet-checksum pkt chk-size) chk-passed)
    chk-passed))

;;; Hoppi application initialization routine
;;;
(defun hoppi-init (application-object)
  "Initialise GUI and I/O."

  ;; Use UTF-8 encoding
  (setf ccl:*default-file-character-encoding* :utf-8)

  ;; Setup application
  (setf *hoppi-application* application-object)

  ;; Setup user defaults
  (setf *user-defaults* (make-instance 'user-defaults))

  ;; Setup main window
  (setf *hoppi-controller* (make-instance 'hoppi-controller))

  ;; Setup main menu
  (load-nibfile (ccl::lisp-string-from-nsstring
                 (#/pathForResource:ofType: (#/mainBundle ns:ns-bundle) #@"hoppi-main-menu" #@"nib"))
                application-object)

  ;; Initialize GUI elements from user defaults
  (let ((ns-path-str (objc:make-nsstring (current-path *user-defaults*))))
    (#/autorelease ns-path-str)
    (#/setURL: (current-path-widget *hoppi-controller*)
               (#/fileURLWithPath:isDirectory: ns:ns-url ns-path-str #$YES)))

  (#/setState: (allow-overwrite-widget *hoppi-controller*) (if (allow-overwrite *user-defaults*) #$NSOnState #$NSOffState))

  (case (connection-type *user-defaults*)
    ((:usb)
     (#/setSelectedSegment: (connection-type-widget *hoppi-controller*) 0)
     (#/removeAllItems (device-path-widget *hoppi-controller*))
     (#/setStringValue: (device-path-widget *hoppi-controller*) #@"")
     (#/setEnabled: (device-path-widget *hoppi-controller*) #$NO))
    ((:serial)
     (#/setSelectedSegment: (connection-type-widget *hoppi-controller*) 1)
     (#/setEnabled: (device-path-widget *hoppi-controller*) #$YES)))

  ;; Start one-second timer
  (setf *one-second-timer* (#/scheduledTimerWithTimeInterval:target:selector:userInfo:repeats:
                            ns:ns-timer 1.0d0 *hoppi-controller* (ccl::@selector "oneSecondTick:") nil #$YES))

  ;; Setup workspace folder chooser
  (setf *folder-chooser* (make-instance 'ns:ns-open-panel))
  (#/setCanChooseDirectories: *folder-chooser* #$YES)
  (#/setCanChooseFiles: *folder-chooser* #$NO)
  (#/setTitle: *folder-chooser* #@"Choose Folder")
  (#/setPrompt: *folder-chooser* #@"Choose")

  ;; Setup file chooser
  (setf *file-chooser* (make-instance 'ns:ns-open-panel))
  (#/setCanChooseDirectories: *file-chooser* #$NO)
  (#/setCanChooseFiles: *file-chooser* #$YES)
  (#/setTitle: *file-chooser* #@"Choose File to Send")
  (#/setPrompt: *file-chooser* #@"Send")

  ;; Setup Kermit protocol controller
  (setf *kpc* (make-instance 'kermit-protocol-controller))

  ;; Setup Kermit receive buffer
  (setf *krxbuf* (make-instance 'kermit-rx-buffer :protocol-controller *kpc*))
  (setf (rx-buffer *kpc*) *krxbuf*)

  ;; Setup Kermit transmit buffer
  (setf *ktxbuf* (make-instance 'kermit-tx-buffer :protocol-controller *kpc*))
  (setf (tx-buffer *kpc*) *ktxbuf*)

  ;; Setup Kermit workspace
  (setf *kwspace* (make-instance 'kermit-workspace :path (current-path *user-defaults*)))
  (setf (workspace *kpc*) *kwspace*)
  (setf (overwrite-action *kwspace*) (if (allow-overwrite *user-defaults*) :supersede nil))

  ;; Setup serial controller
  (setf *serial-controller* (make-instance 'serial-controller))

  ;; Open shared libraries
  (let ((lib-filename (if *lib-path*
                          (concatenate 'string *lib-path* "hoppi-io/libhoppi-io.dylib")
                          (ccl::lisp-string-from-nsstring
                           (#/stringByAppendingPathComponent:
                            (#/bundlePath (#/bundleForClass: ns:ns-bundle
                                                             (#/class application-object)))
                            #@"Contents/Frameworks/lib/libhoppi-io.dylib")))))
    (ccl:open-shared-library lib-filename))

  ;; Setup low-level communications
  (case (connection-type *user-defaults*)
    ((:usb)
     (ccl:external-call "setup_usb_io" :address usb-post-configure-callback :address usb-post-unconfigure-callback :int))
    ((:serial)
     (ccl:external-call "setup_serial_io" :address serial-arrivals-callback :address serial-departures-callback :int))))
