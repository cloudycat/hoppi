;;;
;;; Copyright 2016 Paul Onions
;;;
;;; Licence: MIT, see LICENCE file for details.
;;;
;;; Namestring utilities for the Hoppi system.
;;;
;;; All arguments to these functions should be strings.  All return
;;; values will be strings.  Pathname objects are limited in scope to
;;; internal use only.
;;;
;;; It is the intention that, outside of these functions, all
;;; path-denoting objects in a running Hoppi application are string
;;; objects.
;;;
(in-package :hoppi-utils)

(defun namestring-seperator-p (char)
  (or (eql char #\:) (eql char #\/)))

(defun namestring-components (namestring)
  (let ((nstr (or namestring ""))
        (components nil))
    (loop
       :with start := 0
       :for c :across nstr
       :for n :from 0
       :do (when (or (namestring-seperator-p c)
                     (= n (1- (length nstr))))
             (push (subseq nstr start (1+ n)) components)
             (setf start (1+ n))))
    (reverse components)))

(defun namestring-ends-with-p (namestring end-str)
  (let ((nstr (or namestring "")))
    (and (>= (length nstr) (length end-str))
         (equal (subseq nstr (- (length nstr) (length end-str)))
                end-str))))

(defun namestring-is-folder-p (namestring)
  (let ((nstr (or namestring "")))
    (and (>= (length nstr) 1)
	 (namestring-seperator-p (char nstr (1- (length nstr)))))))

(defun namestring-is-file-p (namestring)
  (let ((nstr (or namestring "")))
    (and (>= (length nstr) 1)
	 (not (namestring-seperator-p (char nstr (1- (length nstr))))))))

(defun namestring-ensure-folder (namestring)
  "Ensure a non-empty folder name ends with a / seperator."
  (let ((nstr (or namestring "")))
    (if (namestring-is-file-p nstr)
	(if (namestring-ends-with-p nstr "/.")
            (subseq nstr 0 (1- (length nstr)))
            (concatenate 'string nstr "/"))
	nstr)))

(defun namestring-ensure-file (namestring)
  "Ensure a non-empty file name does not end with a / seperator."
  (let ((nstr (or namestring "")))
    (loop
      :while (namestring-is-folder-p nstr)
      :do (setf nstr (subseq nstr 0 (1- (length nstr)))))
      nstr))

(defun namestring-folder-part (namestring)
  "Extract the folder part of NAMESTRING."
  (let ((end 0)
        (nstr (or namestring "")))
    (loop
      :for c :across nstr
      :for n :from 0
      :do (when (namestring-seperator-p c)
            (setf end n)))
    (namestring-ensure-folder (subseq nstr 0 end))))

(defun namestring-file-part (namestring)
  "Extract the file part of NAMESTRING."
  (let ((start 0)
        (nstr (or namestring "")))
    (loop
      :for c :across nstr
      :for n :from 0
      :do (when (namestring-seperator-p c)
            (setf start (1+ n))))
    (subseq nstr start)))

(defun namestring-escape (namestring)
  "Escape backslashes and semicolons in the NAMESTRING."
  (let ((nstr (or namestring "")))
    (apply #'concatenate 'string
	   (loop
	      :for c :across nstr
	      :collect (cond ((eql c #\;)
			      "\\;")
			     ((eql c #\\)
			      "\\\\")
			     (t
			      (string c)))))))

(defun namestring-unescape (escaped-namestring)
  "Remove backslash escapes in the ESCAPED-NAMESTRING."
  (let ((enstr (or escaped-namestring "")))
    (coerce (loop
	       :with last-char-dropped := nil
	       :for c :across enstr
	       :when (or (not (eql c #\\))
			 (and (eql c #\\) last-char-dropped))
	       :collect c
	       :do (setf last-char-dropped
			 (and (eql c #\\) (not last-char-dropped))))
	    'string)))

(defun namestring-merge (path defaults)
  "Concatenate folder and file name parts."
  (namestring-unescape
   (namestring
    (merge-pathnames (namestring-escape path)
                     (namestring-escape defaults)))))

(defun namestring-home-folder ()
  "Return the path of the current user's home folder."
  (namestring-unescape (namestring (user-homedir-pathname))))

(defun namestring-list-directory (namestring &key files directories)
  "List contents of given directory."
  (let* ((nstr (or namestring ""))
	 (wildspec (make-pathname :name :wild
				  :type :wild
				  :defaults (namestring-escape
                                             (namestring-ensure-folder nstr))))
         (names nil))
    (loop
       :for pathname :in (directory wildspec :files t :directories t)
       :do (let ((path (namestring-unescape (namestring (probe-file pathname)))))
             (when (or (and directories (namestring-is-folder-p path))
                       (and files (namestring-is-file-p path)))
               (push (car (last (namestring-components path))) names))))
    names))

(defun namestring-probe-file (namestring)
  "Find the truename of the given NAMESTRING.
Will return NIL if the file/folder does not exist."
  (let* ((nstr (or namestring ""))
         (true-pathname (probe-file (namestring-escape nstr))))
    (and true-pathname (namestring-unescape (namestring true-pathname)))))
