;;;
;;; Copyright 2013 - 2017 Paul Onions
;;;
;;; Licence: MIT, see LICENCE file for details.
;;;
;;; Debug utilities for the Hoppi system.
;;;
(in-package :hoppi-utils)

(defvar cl-user::*hoppi-dbg-log* nil
  "Hoppi debug log pathname.
The path of the output file to which DBG-MSG debug messages will be
sent, or set to NIL for no debug messages.")

(defvar *hoppi-dbg-stream* nil)

(defmacro dbg-msg (format-string &rest format-args)
  "Send a debug message.
Note that CL-USER::*HOPPI-DBG-LOG* should be non-NIL before this
macro is evaluated, otherwise it will expand to NIL.  This is
intentional, so that normal builds do not include debug code."
  (when cl-user::*hoppi-dbg-log*
    `(progn
       (unless *hoppi-dbg-stream*
         (setf *hoppi-dbg-stream* (open cl-user::*hoppi-dbg-log*
                                        :direction :output
                                        :if-exists :supersede))
         (multiple-value-bind (second minute hour date month year)
             (get-decoded-time)
           (format *hoppi-dbg-stream* "~D/~D/~D ~2,'0D:~2,'0D:~2,'0D~%~%"
                   year month date hour minute second))
         (force-output *hoppi-dbg-stream*))
       (format *hoppi-dbg-stream* ,format-string ,@format-args)
       (terpri *hoppi-dbg-stream*)
       (force-output *hoppi-dbg-stream*))))
