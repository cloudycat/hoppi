;;;
;;; Copyright 2012 - 2016 Paul Onions
;;;
;;; Licence: MIT, see LICENCE file for details.
;;;
;;; System definition for the core logic of the Hoppi application.
;;;
(in-package :cl-user)

(asdf:defsystem "hoppi-core"
  :description "Hoppi core logic"
  :author "Paul Onions <paul.onions@acm.org>"
  :components ((:file "packages")
               (:file "dbg-utils")
               (:file "namestring-utils")
               (:file "char-map")
               (:file "workspace")
               (:file "kermit"))
  :serial t)
