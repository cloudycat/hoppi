;;;
;;; Copyright 2012 - 2016 Paul Onions
;;;
;;; Licence: MIT, see LICENCE file for details.
;;;
(in-package :hoppi-kermit)

(defparameter *irregular-codes*
  '(( 31  nil   #\U+2026)   ; ellipsis              --> HORIZONTAL ELLIPSIS
    (127  nil   #\U+2592)   ; medium shade          --> MEDIUM SHADE
    (128 "\<)"  #\U+2220)   ; angle                 --> ANGLE
    (129 "\x-"  #\U+0101)   ; x bar                 --> LATIN SMALL LETTER A WITH MACRON
    (130 "\.V"  #\U+2207)   ; gradient              --> NABLA
    (131 "\v/"  #\U+221A)   ; square root           --> SQUARE ROOT
    (132 "\.S"  #\U+222B)   ; integration           --> INTEGRAL
    (133 "\GS"  #\U+03A3)   ; Sigma                 --> GREEK CAPITAL LETTER SIGMA
    (134 "\|>"  #\U+25B6)   ; store                 --> BLACK RIGHT-POINTING TRIANGLE
    (135 "\pi"  #\U+03C0)   ; pi                    --> GREEK SMALL LETTER PI
    (136 "\.d"  #\U+2202)   ; derivation            --> PARTIAL DIFFERENTIAL
    (137 "\<="  #\U+2264)   ; less than or equal    --> LESS-THAN OR EQUAL TO
    (138 "\>="  #\U+2265)   ; greater than or equal --> GREATER-THAN OR EQUAL TO
    (139 "\=/"  #\U+2260)   ; not equal             --> NOT EQUAL TO
    (140 "\Ga"  #\U+03B1)   ; alpha                 --> GREEK SMALL LETTER ALPHA
    (141 "\->"  #\U+2192)   ; right arrow           --> RIGHTWARDS ARROW
    (142 "\<-"  #\U+2190)   ; left arrow            --> LEFTWARDS ARROW
    (143 "\|v"  #\U+2193)   ; down arrow            --> DOWNWARDS ARROW
    (144 "\|^"  #\U+2191)   ; up arrow              --> UPWARDS ARROW
    (145 "\Gg"  #\U+03B3)   ; gamma                 --> GREEK SMALL LETTER GAMMA
    (146 "\Gd"  #\U+03B4)   ; delta                 --> GREEK SMALL LETTER DELTA
    (147 "\Ge"  #\U+03B5)   ; epsilon               --> GREEK SMALL LETTER EPSILON
    (148 "\Gn"  #\U+03B7)   ; eta                   --> GREEK SMALL LETTER ETA
    (149 "\Gh"  #\U+03B8)   ; theta                 --> GREEK SMALL LETTER THETA
    (150 "\Gl"  #\U+03BB)   ; lambda                --> GREEK SMALL LETTER LAMBDA
    (151 "\Gr"  #\U+03C1)   ; rho                   --> GREEK SMALL LETTER RHO
    (152 "\Gs"  #\U+03C3)   ; sigma                 --> GREEK SMALL LETTER SIGMA
    (153 "\Gt"  #\U+03C4)   ; tau                   --> GREEK SMALL LETTER TAU
    (154 "\Gw"  #\U+03C9)   ; omega                 --> GREEK SMALL LETTER OMEGA
    (155 "\GD"  #\U+0394)   ; Delta                 --> GREEK CAPITAL LETTER DELTA
    (156 "\PI"  #\U+03A0)   ; Pi                    --> GREEK CAPTIAL LETTER PI
    (157 "\GW"  #\U+03A9)   ; Omega                 --> GREEK CAPTIAL LETTER OMEGA
    (158 "\[]"  #\U+25A0)   ; box                   --> BLACK SQUARE
    (159 "\oo"  #\U+221E)   ; infinity              --> INFINITY
    (160 "\160" #\U+00A0)   ; space                 --> NON-BREAKING SPACE
    (171 "\<<"  #\U+00AB)   ; begin program         --> LEFT-POINTING DOUBLE ANGLE QUOTATION MARK
    (176 "\^o"  #\U+00B0)   ; degree                --> DEGREE SIGN
    (181 "\Gm"  #\U+00B5)   ; mu                    --> MICRO SIGN
    (187 "\>>"  #\U+00BB)   ; end program           --> RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK
    (215 "\.x"  #\U+00D7)   ; multiplcation         --> MUTLIPLICATION SIGN
    (216 "\O/"  #\U+00D8)   ; O slash               --> LATIN CAPITAL LETTER O WITH STROKE
    (223 "\Gb"  #\U+00DF)   ; beta                  --> LATIN SMALL LETTER SHARP S
    (247 "\:-"  #\U+00F7))) ; division              --> DIVISION SIGN

(defparameter *hp-to-unicode-table*
  (let ((table (make-array 256 :element-type 'character)))
    (loop :for n :from 0 :to 255
          :do (setf (aref table n) (code-char n)))
    (loop :for map :in *irregular-codes*
          :do (setf (aref table (first map)) (third map)))
    table))

(defparameter *unicode-to-hp-table*
  (let ((table (make-hash-table)))
    (loop :for n :from 0 :to 255
          :do (let ((map (first (member n *irregular-codes* :test (lambda (n map) (eql n (first map)))))))
                (setf (gethash (if map (third map) (code-char n)) table) n)))
    table))

(defun hp-code-to-unicode-char (byte)
  "Translate an HP character code to a Unicode character."
  (aref *hp-to-unicode-table* (logand byte #xFF)))

(defun unicode-char-to-hp-code (char)
  "Translate a Unicode character to an HP character code."
  (let ((byte (gethash char *unicode-to-hp-table*)))
    (if byte byte (char-code #\?))))
